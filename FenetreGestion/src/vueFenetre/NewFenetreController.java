package vueFenetre;

import java.io.File;
import java.util.ArrayList;

import dao.ParametreBean;
import fenetreGestion.MenuApplication;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import service.Repertoire;

public class NewFenetreController {
	private MenuApplication menuApp;
	private ParametreBean parametres;
	private ArrayList<String> recentFiles;
	@FXML
	Menu menuFiles;

	@FXML
	private MenuItem about;

	@FXML
	private MenuItem saveButton;

	@FXML
	private MenuItem quit;

	public void setMenuApp(MenuApplication menuApp) {
		this.menuApp = menuApp;
		parametres = menuApp.getParameter();
		recentFiles = parametres.getRecentFiles();
		generateRecentFiles();
	}

	public void aboutWindow() {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Help");
		alert.setHeaderText("Application Test");
		alert.setContentText("HELP");
		alert.showAndWait();
	}

	public void openFile() {
		menuApp.sauver();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open a file");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Text Files", "*.txt"));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Csv Files", "*.csv"));
		fileChooser.setInitialDirectory(new File(menuApp.getParameter().getDernierRepertoire()));
		File selectedFile = fileChooser.showOpenDialog(menuApp.getStage());
		System.out.println(selectedFile);
		if (selectedFile != null)

			open(selectedFile);
	}

	public void newFile() {
		menuApp.sauver();
		FileChooser newChooser = new FileChooser();
		newChooser.setTitle("Open Resource file");
		newChooser.getExtensionFilters().addAll(new ExtensionFilter("Text Files", "*.txt"));
		newChooser.setInitialDirectory(new File(menuApp.getParameter().getDernierRepertoire()));
		File selectedFile = newChooser.showSaveDialog(menuApp.getStage());
		if (selectedFile != null) {
			menuApp.setRepertoire(new Repertoire(selectedFile));
		}
	}

	private void updateParameters(File selectedFile) {

		String file = selectedFile.getAbsolutePath();
		parametres.setLastFolder(file);

		int pos = parametres.getRecentFiles().indexOf(file);
		if (pos != -1) {

			parametres.getRecentFiles().remove(pos);
		}
		parametres.getRecentFiles().add(0, selectedFile.getAbsolutePath());
		parametres.save();
		generateRecentFiles();

	}

	private void generateRecentFiles() {
		menuFiles.getItems().clear();

		for (int i = 0; i < recentFiles.size(); i++) {
			menuFiles.getItems().add(new MenuItem(recentFiles.get(i)));
			menuFiles.getItems().get(i).setOnAction(action -> openRecentFiles(action));
		}
	}

	private void openRecentFiles(ActionEvent action) {
		MenuItem menuItem = (MenuItem) action.getSource();
		File selectedFile = new File(menuItem.getText());
		if (selectedFile.exists()) {

			open(selectedFile);

		} else {

			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur : Fichier absent");
			alert.setHeaderText(selectedFile.getPath());
			alert.setContentText(
					"Le fichier n'est plus accessible, il a certainement �t� supprim� depuis la derni�re session !!!");
			alert.showAndWait();

		}
	}

	private void open(File selectedFile) {

		try {
			Repertoire repertoire = new Repertoire(selectedFile);
			menuApp.setRepertoire(repertoire);
			updateParameters(selectedFile);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur : Format incorrect");
			alert.setHeaderText(selectedFile.getPath());
			alert.setContentText("Le fichier n'est pas au bon format !");
			alert.showAndWait();
		}

	}

	public void exitApp() {
		menuApp.sauver();
		Platform.exit();
	}

	public void closeApp() {
		menuApp.sauver();
		menuApp.closeApp();
	}

	public void saveApp() {

		menuApp.getRepertoire().sauver();
	}

	@FXML
	void saveAs(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Contacts Files", "*.contact"));
		fileChooser.setInitialDirectory(new File(menuApp.getParameter().getDernierRepertoire()));
		File selectedFile = fileChooser.showSaveDialog(menuApp.getStage());
		if (selectedFile != null) {
			updateParameters(selectedFile);
			menuApp.getRepertoire().setFile(selectedFile);
		}
	}

}
