package outils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateTools {

	private static DateTimeFormatter dateNum = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	private static DateTimeFormatter dateChaine = DateTimeFormatter.ofPattern("d MMMM yyyy");

	public static LocalDate stringToDate(String date) {

		return LocalDate.parse(date, dateNum);
	}

	public static String dateToString(LocalDate date) {

		return dateNum.format(date);
	}

	public static String dateToLitteral(LocalDate date) {

		return dateChaine.format(date);
	}

}
