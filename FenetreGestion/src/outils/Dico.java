package outils;

import java.util.ArrayList;
import java.util.Random;

public class Dico {
	private final String chemin = "C:\\JAVA\\Dictionnaires\\";
	private String langue;
	private ArrayList<String> mots;

	public Dico(String langue) {
		this.langue = langue;
		String fichier = chemin + langue + ".txt";
		mots = (ArrayList<String>) new LectureFichier(fichier).lire();
	}

	public String genererMotAleatoire() {
		Random alea = new Random();
		return mots.get(alea.nextInt(mots.size()));
	}

}
