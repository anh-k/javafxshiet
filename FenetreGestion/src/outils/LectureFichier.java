package outils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LectureFichier
{
	String nomFichier;
	public LectureFichier (String nomFichier)
	{
		this.nomFichier = nomFichier;
	}
	
	public List <String> lire()
	{
		ArrayList<String> lignes = new ArrayList<>();
		try (BufferedReader fichier = new BufferedReader(new FileReader(nomFichier)))
		{	
			String ligne;
			while((ligne = fichier.readLine()) != null)
			{
				lignes.add(ligne);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return lignes;
	}
}
