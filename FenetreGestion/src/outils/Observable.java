package outils;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable {

	private List<Observer> observers = new ArrayList<>();

	public void add(Observer observer) {

		observers.add(observer);

	}

	public void remove(Observer observer) {

		observers.remove(observer);
	}

	public void notifier() {

		for (Observer observer : observers)

			observer.actualise(this);

	}
}