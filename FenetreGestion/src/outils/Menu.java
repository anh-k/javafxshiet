package outils;

public class Menu
{
	private String titre;
	private String [] options;
	
	public Menu (String titre , String [] options)
	{
		this.titre = titre.toUpperCase();
		this.options = options;
	}
	
	public int choisirUneOption()
	{
		System.out.println(Chaine.repeter(" " , 10 ) + Chaine.repeter(".", titre.length()));
		System.out.println(Chaine.repeter(" " , 10) + titre);
		System.out.println(Chaine.repeter(" " , 10 ) + Chaine.repeter(".", titre.length()));
		System.out.println();
		for(int i = 0 ; i < options.length;i++ )
		{
			System.out.printf("%d - %s%n",i,options[i]);
		}
		System.out.println();
		return Saisie.saisirEntier("Your choice" , 0 , options.length - 1);
	}
}
