package outils;

public interface Observer {

	void actualise(Observable observable);

}
