package outils;

import java.util.Scanner;

public class Saisie {
	static Scanner monScanner = new Scanner(System.in);

	private Saisie() {

	}

	public static String saisirUneChaine(String message) {
		System.out.printf("%s : ", message);
		return monScanner.nextLine();
	}

	public static int saisirEntier(String message) {
		while (true) {
			try {
				System.out.printf("%s : ", message);
				return Integer.parseInt(monScanner.nextLine());
			} catch (Exception e) {
				System.out.println("Erreur : " + e.getMessage());
			}
		}
	}

	public static String saisirChar(String message) {
		while (true) {
			try {
				System.out.printf("%s : ", message);
				return monScanner.next().substring(0);
			} catch (Exception e) {
				System.out.println("Erreur : " + e.getMessage());
			}
		}
	}

	public static int saisirEntier(String message, int min, int max) {
		while (true) {
			try {
				System.out.printf("%s (%d-%d) : ", message, min, max);
				int resultat = Integer.parseInt(monScanner.nextLine());
				if (resultat >= min && resultat <= max) {
					return resultat;
				}
			} catch (Exception e) {
				System.out.println("Erreur : " + e.getMessage());
			}
		}
	}

}
