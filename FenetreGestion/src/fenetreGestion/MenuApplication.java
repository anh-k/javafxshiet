package fenetreGestion;

import java.io.IOException;
import java.util.Optional;

import dao.ParametreBean;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Personne;
import service.Repertoire;
import vue.ContactsController;
import vue.SaisieContactController;
import vue.SaisieEnfantController;
import vueFenetre.NewFenetreController;

public class MenuApplication extends Application {

	private Stage stage;
	private BorderPane menuLayout;
	private Repertoire repertoire;
	private ParametreBean parameters;
	private Scene contactsScene;
	private Stage modalStage;

	public static void main(String[] args) {

		launch(args);
	}

	@Override
	public void start(Stage stage) {
		this.stage = stage;
		this.stage.setTitle("Main Menu");
		stage.setOnCloseRequest(event -> sauver()	);
		parameters = new ParametreBean();
		showMainWindow();

	}

	public ParametreBean getParameter() {
		return parameters;
	}

	public BorderPane getMenuLayout() {
		return menuLayout;
	}

	public Repertoire getRepertoire() {
		return repertoire;
	}

	public void setRepertoire(Repertoire repertoire) {
		this.repertoire = repertoire;
		showGestionContacts();
	}

	public Stage getStage() {
		return stage;
	}

	private void showMainWindow() {
		try {

			// Load FXML
			FXMLLoader newLoader = new FXMLLoader();
			newLoader.setLocation(MenuApplication.class.getResource("../vueFenetre/NewFenetre.fxml"));
			menuLayout = newLoader.load();

			// Init layout
			Scene scene = new Scene(menuLayout, 1200, 525);
			scene.getStylesheets().add(getClass().getResource("./menuapp.css").toExternalForm());
			stage.setScene(scene);
			stage.setResizable(false);

			// D�claration et instanciation du controller
			NewFenetreController fenetreController = newLoader.getController();
			fenetreController.setMenuApp(this);

			// Afficher le stage
			stage.show();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void showGestionContacts() {

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MenuApplication.class.getResource("../vue/Contacts.fxml"));
			AnchorPane contactsOverview = loader.load();

			menuLayout.setCenter(contactsOverview);

			ContactsController contactsController = loader.getController();
			contactsController.setMainApp(this);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean showModifContact() {
		try {

			// Load FXML File
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MenuApplication.class.getResource("../vue/SaisieContact.fxml"));
			AnchorPane anchorPane = loader.load();

			// Create the dialog stage

			modalStage = new Stage();
			modalStage.setTitle("Edit Person");
			modalStage.initModality(Modality.WINDOW_MODAL);
			modalStage.initOwner(stage);
			contactsScene = new Scene(anchorPane);
			contactsScene.getStylesheets().add(getClass().getResource("../vue/SaisieContact.css").toExternalForm());
			modalStage.setScene(contactsScene);
			modalStage.setResizable(false);

			// Parameters
			SaisieContactController controller = loader.getController();
			controller.setDialogStage(modalStage);
			controller.setMain(this);
			controller.setEmployee(repertoire.getSelectedEmployee());

			// Show the dialog stage and wait

			modalStage.showAndWait();
			return controller.isOkClicked();

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	public boolean showModifEnfant() {
		try {

			// Load FXML File

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MenuApplication.class.getResource("../vue/SaisieEnfant.fxml"));
			AnchorPane anchorPane = loader.load();

			// Create the dialog stage

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit Children");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			Scene scene = new Scene(anchorPane);
			dialogStage.initOwner(modalStage);
			dialogStage.setScene(scene);
			dialogStage.setResizable(false);

			// Parameters

			SaisieEnfantController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setMain(this);
			controller.setEnfant(repertoire.getSelectedEnfant());

			// Show the dialog stage and wait

			dialogStage.showAndWait();
			return controller.isOkClicked();

		} catch (IOException e) {
			return false;
		}

	}

	public void sauver() {

		if (repertoire != null && !repertoire.isSaved()) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Modifications d�tect�es !");
			alert.setContentText(String.format(
					"Le fichier %s a �t� modifi�, voulez-vous enregistrer les modifications ? ", repertoire.getFile()));
			Optional<ButtonType> result = alert.showAndWait();
			if (result.isPresent() && result.get() == ButtonType.OK) {
				repertoire.sauver();
			}
		}
	}

	public void closeApp() {
		sauver();
		menuLayout.setCenter(null);
	}
}
