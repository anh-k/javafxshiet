package service;

import model.Agence;
import model.Employee;
import model.EmployeesRights;
import model.Personne;

import java.io.File;
import dao.ContactsDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;

/**
 * attributs et m�thodes permettant de d�finir un r�pertoire
 * 
 * @author Fran�ois
 *
 */
public class Repertoire {
	private ObservableList<Employee> allContacts;
	private FilteredList<Employee> filteredContacts;
	private SortedList<Employee> sortedContacts;
	private Employee selectedEmployee;
	private Personne selectedEnfant;
	private File file;
	private boolean saved;
	private ContactsDAO contactsDAO;
	private Employee searchedEmployee;

	public Repertoire(File file) {

		searchedEmployee = new Employee("", "", "01/01/2001", "");
		this.file = file;
		contactsDAO = new ContactsDAO(file);
		allContacts = FXCollections.observableArrayList(contactsDAO.lire());
		filteredContacts = new FilteredList<>(allContacts, null);
		sortedContacts = new SortedList<>(filteredContacts);
		saved = true;
	}

	public Employee getSearchedEmployee() {
		return searchedEmployee;
	}

	public void setSearchedEmployee(Employee searchedEmployee) {
		this.searchedEmployee = searchedEmployee;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public File getFile() {
		return file;
	}

	public boolean isSaved() {
		return saved;
	}

	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}

	public void setSelectedEmployee(Employee selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
	}

	public SortedList<Employee> getContacts() {
		return sortedContacts;

	}

	public void setNomSearched(String nomSearched) {
		searchedEmployee.setNom(nomSearched);
		filtrerContact();
	}

	public void setPrenomSearched(String prenomSearched) {
		searchedEmployee.setPrenom(prenomSearched);
		filtrerContact();
	}

	public void setSexeSearched(String sexeSearched) {
		searchedEmployee.setSexe(sexeSearched);
		filtrerContact();
	}

	public void setAgenceSearched(Agence agenceSearched) {
		searchedEmployee.setAgence(agenceSearched);
		filtrerContact();
	}

	public void ajouter() {
		if (allContacts.indexOf(selectedEmployee) == -1)
			allContacts.add(selectedEmployee);
		saved = false;
	}

	public void supprimer() {
		allContacts.remove(selectedEmployee);
		saved = false;
	}

	public void supprimerEnfant() {
		selectedEmployee.getEnfants().remove(selectedEnfant);

	}

	public void addEnfant() {
		if (selectedEmployee.getEnfants().indexOf(selectedEnfant) == -1)
			selectedEmployee.getEnfants().add(selectedEnfant);
		saved = false;
	}

	public void sauver() {
		contactsDAO.sauver(allContacts);
		saved = true;
	}

	public void setSaved(boolean saved) {
		this.saved = this.saved && saved;
	}

	public void filtrerContact() {
		filteredContacts.setPredicate(employee -> {
			boolean c1 = employee.getNom().contains(searchedEmployee.getNom().toUpperCase());
			boolean c2 = employee.getPrenom().toLowerCase().contains(searchedEmployee.getPrenom().toLowerCase());
			boolean c3 = employee.getSexe().contains(searchedEmployee.getSexe());
			boolean c4 = searchedEmployee.getAgence().getNom().equalsIgnoreCase("Agence") ? true
					: employee.getAgence().equals(searchedEmployee.getAgence());

			return c1 && c2 & c3 && c4;
		});
	}

	public Personne getSelectedEnfant() {
		return selectedEnfant;
	}

	public void setSelectedEnfant(Personne selectedEnfant) {
		this.selectedEnfant = selectedEnfant;
	}
}
