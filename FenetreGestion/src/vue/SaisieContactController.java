package vue;

import model.Agence;
import model.Employee;
import model.Personne;
import outils.DateTools;
import service.Repertoire;

import java.time.LocalDate;
import java.util.Optional;

import fenetreGestion.MenuApplication;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.stage.Stage;

public class SaisieContactController {
	private Stage dialogStage;
	private Employee employee;

	private boolean okClicked;
	@FXML
	private RadioButton femmeButton;
	@FXML
	private RadioButton hommeButton;
	@FXML
	private DatePicker pickerDateNaissance;
	@FXML
	private DatePicker pickerDateEmbauche;
	@FXML
	private TextField textFieldPrenom;
	@FXML
	private TextField textFieldNom;
	@FXML
	private TableView<Personne> enfantsTable;
	@FXML
	private TableColumn<Personne, String> prenomColumn;
	@FXML
	private TableColumn<Personne, String> dateDeNaissanceColumn;
	@FXML
	private TableColumn<Personne, String> sexeColumn;
	@FXML
	private ComboBox<Agence> agenceBox;
	@FXML
	private ButtonBar buttonModifierSupprimer;
	@FXML
	private Button buttonModifier;
	@FXML
	private Button buttonSupprimer;
	@FXML
	private MenuItem supprMenuItem;
	@FXML
	private MenuItem editMenuItem;
	private Repertoire repertoire;
	private MenuApplication menuApp;

	@FXML
	private void initialize() {

		disableEditSupprButtons(true);
		prenomColumn.setCellValueFactory(
				(CellDataFeatures<Personne, String> feature) -> feature.getValue().prenomProperty());

		dateDeNaissanceColumn.setCellValueFactory(
				(CellDataFeatures<Personne, String> feature) -> feature.getValue().dateNaissanceProperty());

		sexeColumn
				.setCellValueFactory((CellDataFeatures<Personne, String> feature) -> feature.getValue().sexeProperty());

		enfantsTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> selectionnerEnfant(newValue));

	}

	private void selectionnerEnfant(Personne enfant) {

		repertoire.setSelectedEnfant(enfant);
		disableEditSupprButtons(false);
	}

	@FXML
	private void disableEditSupprButtons(boolean disable) {
		buttonModifierSupprimer.setDisable(disable);
//		editMenuItem.setDisable(disable);
//		supprMenuItem.setDisable(disable);
	}

	public void setEmployee(Employee employee) {

		this.employee = employee;
		textFieldNom.setText(employee.getNom());
		textFieldPrenom.setText(employee.getPrenom());
		pickerDateNaissance.setValue(DateTools.stringToDate(employee.getDateNaissance()));
		pickerDateEmbauche.setValue(DateTools.stringToDate(employee.getDateEmbauche()));
		selectSexe(employee);
		setPickerEmbauche();
		setPickerNaissance();
		agenceBox.setValue(employee.getAgence());
		enfantsTable.setItems(employee.getEnfants());
		generateAgence();
		repertoire.getContacts().comparatorProperty().bind(enfantsTable.comparatorProperty());

	}

	@FXML
	private void setPickerEmbauche() {
		pickerDateEmbauche.setDayCellFactory(de -> new DateCell() {
			@Override
			public void updateItem(LocalDate item, boolean empty) {
				super.updateItem(item, empty);
				setDisable(
						item.isBefore(pickerDateNaissance.getValue().plusYears(16)) || item.isAfter(LocalDate.now()));

				setTooltip(new Tooltip(DateTools.dateToLitteral(item)));
			}
		});
	}

	@FXML
	private void setPickerNaissance() {
		pickerDateNaissance.setDayCellFactory(dn -> new DateCell() {
			@Override
			public void updateItem(LocalDate item, boolean empty) {
				super.updateItem(item, empty);
				setDisable(item.isAfter(LocalDate.now()));
				setTooltip(new Tooltip(DateTools.dateToLitteral(item)));
			}
		});

	}

	public void setMain(MenuApplication menuApp) {
		this.menuApp = menuApp;
		repertoire = menuApp.getRepertoire();

	}

	@FXML
	private void ajouterEnfant() {

		repertoire.setSelectedEnfant(new Personne("", "", DateTools.dateToString(LocalDate.now()), ""));
		if (menuApp.showModifEnfant()) {
			repertoire.addEnfant();
			enfantsTable.getSelectionModel().select(null);
		}
	}

	@FXML
	private void modifierEnfant() {

		repertoire.setSaved(!menuApp.showModifEnfant());

	}

	@FXML
	private void supprimer() {
		Alert alert = new Alert(AlertType.CONFIRMATION);

		alert.setTitle("Suppression d'un employ�");
		alert.setContentText("Etes-vous s�r de vouloir supprimer " + repertoire.getSelectedEnfant());
		Optional<ButtonType> result = alert.showAndWait();

		if (result.isPresent() && result.get() == ButtonType.OK) {

			repertoire.supprimerEnfant();
			enfantsTable.getSelectionModel().select(null);
			buttonModifierSupprimer.setDisable(true);
			okClicked = true;
		}

	}

	public void generateAgence() {

		agenceBox.getItems().add(new Agence("Lille"));
		agenceBox.getItems().add(new Agence("Roubaix"));
		agenceBox.getItems().add(new Agence("Dunkerque"));

	}

	private void selectSexe(Employee employee) {
		if (employee.getSexe().equals("Masculin")) {
			hommeButton.setSelected(true);
			hommeButton.requestFocus();

		} else if (employee.getSexe().equals("F�minin")) {
			femmeButton.setSelected(true);
			femmeButton.requestFocus();
		}
	}

	private String sexeSelected(Employee employee) {
		String sexe = "";
		if (hommeButton.isSelected()) {
			sexe = "Masculin";
			employee.setSexe(sexe);
		} else if (femmeButton.isSelected()) {
			sexe = "F�minin";
			employee.setSexe(sexe);
		}
		return sexe;
	}

	@FXML
	void annuler() {
		okClicked = false;
		dialogStage.close();
	}

	@FXML
	void enregister() {
		if (textFieldNom.getText().trim().equals("") || textFieldPrenom.getText().trim().equals("")
				|| pickerDateNaissance.getValue() == null) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur de saisie");
			alert.setContentText("Veuillez compl�ter l'ensemble des champs de saisie");
			alert.showAndWait();
		} else {

			okClicked = !employee.getNom().equals(textFieldNom.getText())
					|| !employee.getPrenom().equals(textFieldPrenom.getText())
					|| !employee.getDateNaissance().equals(DateTools.dateToString(pickerDateNaissance.getValue()))
					|| !employee.getDateEmbauche().equals(DateTools.dateToString(pickerDateEmbauche.getValue()))
					|| !femmeButton.isSelected() || !hommeButton.isSelected();
			employee.setAll(textFieldNom.getText(), textFieldPrenom.getText(), pickerDateNaissance.getValue(),
					sexeSelected(employee));

			employee.setDateEmbauche(DateTools.dateToString(pickerDateEmbauche.getValue()));
			employee.setAgence(agenceBox.getValue());

			dialogStage.close();
		}
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
		okClicked = false;
	}

	public boolean isOkClicked() {
		return okClicked;
	}

}
