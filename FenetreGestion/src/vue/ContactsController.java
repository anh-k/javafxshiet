package vue;

import java.time.LocalDate;
import java.util.Optional;
import fenetreGestion.MenuApplication;
import model.Agence;
import model.Employee;
import outils.DateTools;
import service.Repertoire;
import javafx.fxml.FXML;
import javafx.print.PrinterJob;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;

public class ContactsController {
	@FXML
	private TableView<Employee> contactsTable;
	@FXML
	private TableColumn<Employee, String> nomColumn;
	@FXML
	private TableColumn<Employee, String> prenomColumn;
	@FXML
	private TableColumn<Employee, String> dateDeNaissanceColumn;
	@FXML
	private TableColumn<Employee, String> sexeColumn;
	@FXML
	private TableColumn<Employee, Agence> agenceColumn;
	@FXML
	private TableColumn<Employee, String> dateEmbaucheColumn;
	@FXML
	private MenuItem supprMenuItem;
	@FXML
	private MenuItem editMenuItem;
	@FXML
	private Label detailNom;
	@FXML
	private Label detailPrenom;
	@FXML
	private Label detailDateDeNaissance;
	@FXML
	private Label detailSexe;
	@FXML
	private Label detailAgence;
	@FXML
	private Label detailDateEmbauche;
	@FXML
	private Label detailEnfants;
	@FXML
	private Label detailResto;
	@FXML
	private Label detailNoel;
	@FXML
	private Label detailVacs;
	@FXML
	private ButtonBar buttonModifierSupprimer;
	@FXML
	private Button buttonModifier;
	@FXML
	private Button buttonSupprimer;
	@FXML
	private ContextMenu menuContext;
	@FXML
	private ComboBox<Agence> agenceBoxSearch;
	@FXML
	private RadioButton tousButton;
	@FXML
	private RadioButton mButton;
	@FXML
	private RadioButton fButton;
	@FXML
	private RadioButton noelButton;
	@FXML
	private RadioButton vacancesButton;
	private MenuApplication mainApplication;
	private Repertoire repertoire;
	@FXML
	private TextField textFieldNomRecherche;
	@FXML
	private TextField textFieldPrenomRecherche;

	@FXML
	private void initialize() {

		griserContextMenu(true);

		nomColumn.setCellValueFactory((CellDataFeatures<Employee, String> feature) -> feature.getValue().nomProperty());

		prenomColumn.setCellValueFactory(
				(CellDataFeatures<Employee, String> feature) -> feature.getValue().prenomProperty());

		dateDeNaissanceColumn.setCellValueFactory(
				(CellDataFeatures<Employee, String> feature) -> feature.getValue().dateNaissanceProperty());

		dateEmbaucheColumn.setCellValueFactory(
				(CellDataFeatures<Employee, String> feature) -> feature.getValue().dateEmbaucheProperty());

		agenceColumn.setCellValueFactory(
				(CellDataFeatures<Employee, Agence> feature) -> feature.getValue().agenceProperty());

		sexeColumn
				.setCellValueFactory((CellDataFeatures<Employee, String> feature) -> feature.getValue().sexeProperty());

		// Ajout d'un ecouteur sur les items
		contactsTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> afficherDetailContact(newValue));

		textFieldNomRecherche.textProperty()
				.addListener((observable, oldValue, newValue) -> repertoire.setNomSearched(newValue));

		textFieldPrenomRecherche.textProperty()
				.addListener((observable, oldValue, newValue) -> repertoire.setPrenomSearched(newValue));

	}

	@FXML
	private void griserContextMenu(boolean correct) {

		editMenuItem.setDisable(correct);
		supprMenuItem.setDisable(correct);
	}

	@FXML
	private void supprimer() {
		Alert alert = new Alert(AlertType.CONFIRMATION);

		alert.setTitle("Suppression d'un employ�");
		alert.setContentText("Etes-vous s�r de vouloir supprimer " + repertoire.getSelectedEmployee());
		Optional<ButtonType> result = alert.showAndWait();

		if (result.isPresent() && result.get() == ButtonType.OK) {

			repertoire.supprimer();
			contactsTable.getSelectionModel().select(null);

			griserContextMenu(true);
		}

	}

	@FXML
	private void imprimer() {
		PrinterJob printerJob = PrinterJob.createPrinterJob();
		printerJob.showPrintDialog(mainApplication.getStage());
		boolean success = printerJob.printPage(contactsTable);
		if (success) {
			printerJob.endJob();

		} else
			printerJob.cancelJob();

	}

	@FXML
	private void modifier() {

		repertoire.setSaved(!mainApplication.showModifContact());

	}

	@FXML
	private void ajouter() {

		repertoire
				.setSelectedEmployee(new Employee("", "", DateTools.dateToString(LocalDate.now().minusYears(16)), ""));
		repertoire.getSelectedEmployee().setDateEmbauche(DateTools.dateToString(LocalDate.now()));
		if (mainApplication.showModifContact()) {
			repertoire.ajouter();
			contactsTable.getSelectionModel().select(null);
		}
	}

	@FXML
	private void afficherDetailContact(Employee employee) {
		repertoire.setSelectedEmployee(employee);
		if (employee != null) {
			detailNom.setText(employee.getNom());
			detailPrenom.setText(employee.getPrenom());
			detailDateDeNaissance.setText(employee.getDateNaissance());
			detailSexe.setText(employee.getSexe());
			detailDateEmbauche.setText(employee.getDateEmbauche());
			detailAgence.setText(employee.getAgence().getNom());
			detailResto.setText(employee.getRights().afficherResto());
			detailNoel.setText(employee.getRights().afficherChequesNoel());
			detailVacs.setText(employee.getRights().afficherChequesVacs());
			buttonModifierSupprimer.setDisable(false);
			griserContextMenu(false);

		} else {
			detailNom.setText("");
			detailPrenom.setText("");
			detailDateDeNaissance.setText("");
			detailSexe.setText("");
			detailDateEmbauche.setText("");
			detailAgence.setText("");
			detailResto.setText("");
			detailNoel.setText("");
			detailVacs.setText("");
			buttonModifierSupprimer.setDisable(true);
			griserContextMenu(true);
		}

	}

	public void setMainApp(MenuApplication menuApp) {
		this.mainApplication = menuApp;
		repertoire = menuApp.getRepertoire();
		contactsTable.setItems(repertoire.getContacts());
		repertoire.getContacts().comparatorProperty().bind(contactsTable.comparatorProperty());
		Agence agence = new Agence("Agence");
		agenceBoxSearch.getItems().add(agence);
		agenceBoxSearch.getSelectionModel().select(agence);
		agenceBoxSearch.getItems().add(new Agence("Lille"));
		agenceBoxSearch.getItems().add(new Agence("Roubaix"));
		agenceBoxSearch.getItems().add(new Agence("Dunkerque"));
		tousButton.setSelected(true);

	}

	@FXML
	public void filterEmployees() {

		repertoire.setAgenceSearched(agenceBoxSearch.getSelectionModel().getSelectedItem());

		if (tousButton.isSelected())
			repertoire.setSexeSearched("");
		else if (fButton.isSelected())
			repertoire.setSexeSearched("F�minin");
		else if (mButton.isSelected())
			repertoire.setSexeSearched("Masculin");

	}

}
