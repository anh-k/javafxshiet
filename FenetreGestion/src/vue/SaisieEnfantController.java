package vue;

import java.time.LocalDate;
import java.util.ArrayList;

import fenetreGestion.MenuApplication;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.Employee;
import model.Personne;
import outils.DateTools;
import service.Repertoire;

public class SaisieEnfantController {
	private boolean okClicked;
	private Stage dialogStage;
	private Personne enfant;
	private Repertoire repertoire;
	private MenuApplication menuApp;
	private Employee parent;
	private ObservableList<Personne> siblings;
	@FXML
	private DatePicker pickerDateNaissance;
	@FXML
	private TextField textFieldPrenom;
	@FXML
	private RadioButton femmeButton;
	@FXML
	private RadioButton hommeButton;

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
		okClicked = false;
	}

	public void setEnfant(Personne enfant) {

		parent = repertoire.getSelectedEmployee();
		siblings = parent.getEnfants();
		repertoire.setSelectedEnfant(enfant);
		this.enfant = enfant;
		textFieldPrenom.setText(enfant.getPrenom());
		pickerDateNaissance.setValue(DateTools.stringToDate(enfant.getDateNaissance()));
		selectSexe(enfant);
		setPickerNaissance();
	}

	@FXML
	private void setPickerNaissance() {
		pickerDateNaissance.setDayCellFactory(dn -> new DateCell() {
			@Override
			public void updateItem(LocalDate item, boolean empty) {
				super.updateItem(item, empty);
				setDisable(item.isBefore(DateTools.stringToDate(parent.getDateNaissance()).plusYears(16))
						|| item.isAfter(LocalDate.now()));
				setTooltip(new Tooltip(DateTools.dateToLitteral(item)));
			}
		});

	}

	@FXML
	void annuler() {
		okClicked = false;
		dialogStage.close();
	}

	public boolean isOkClicked() {

		return okClicked;
	}

	@FXML
	void enregister() {
		if (textFieldPrenom.getText().trim().equals("") || pickerDateNaissance.getValue() == null
				|| !femmeButton.isSelected() && !hommeButton.isSelected()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur de saisie");
			alert.setContentText("Veuillez compl�ter l'ensemble des champs de saisie");
			alert.showAndWait();
		} else {

			okClicked = !enfant.getPrenom().equals(textFieldPrenom.getText())
					|| !enfant.getDateNaissance().equals(DateTools.dateToString(pickerDateNaissance.getValue()))
					|| !femmeButton.isSelected() || !hommeButton.isSelected();
			enfant.setAll(repertoire.getSelectedEmployee().getNom(), textFieldPrenom.getText(),
					pickerDateNaissance.getValue(), sexeSelected(enfant));

			dialogStage.close();
		}
	}

	private String sexeSelected(Personne enfant) {
		String sexe = "";
		if (hommeButton.isSelected()) {
			sexe = "Masculin";
			enfant.setSexe(sexe);
		} else if (femmeButton.isSelected()) {
			sexe = "F�minin";
			enfant.setSexe(sexe);
		}
		return sexe;
	}

	private void selectSexe(Personne enfant) {
		if (enfant.getSexe().equals("Masculin")) {
			hommeButton.setSelected(true);
			hommeButton.requestFocus();

		} else if (enfant.getSexe().equals("F�minin")) {
			femmeButton.setSelected(true);
			femmeButton.requestFocus();
		}
	}

	public void setMain(MenuApplication menuApp) {
		this.menuApp = menuApp;
		repertoire = menuApp.getRepertoire();

	}
}
