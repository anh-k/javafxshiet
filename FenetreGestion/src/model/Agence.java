package model;

public class Agence {
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + (restaurant ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agence other = (Agence) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (restaurant != other.restaurant)
			return false;
		return true;
	}

	private String nom;
	private boolean restaurant;

	public Agence(String nom) {
		super();
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void ouvrirRestaurant() {
		restaurant = true;
	}

	public void fermerRestaurant() {
		restaurant = false;
	}

	boolean isRestaurant() {
		return restaurant;
	}

	@Override
	public String toString() {
		return nom;
	}

}