package model;

import java.time.LocalDate;

import outils.DateTools;

public class EmployeesRights {
	private static final int MOIS_ANCIENNETE = 18;
	private static final String BASE_ANCIENNETE = "01/07/";
	private static final int AGE1 = 10;
	private static final int MONTANT1 = 20;
	private static final int AGE2 = 15;
	private static final int MONTANT2 = 30;
	private static final int AGE3 = 18;
	private static final int MONTANT3 = 50;

	private Employee employee;

	public EmployeesRights(Employee employee) {
		this.employee = employee;
	}

	private boolean chequesVacances() {
		LocalDate base = DateTools.stringToDate(BASE_ANCIENNETE + LocalDate.now().getYear());
		return base.minusMonths(MOIS_ANCIENNETE).compareTo(DateTools.stringToDate(employee.getDateEmbauche())) >= 0;
	}

	private int chequesNoel() {
		int montant = 0;
		for (int i = 0; i < employee.getEnfants().size(); i++) {
			int age = LocalDate.now().getYear()
					- DateTools.stringToDate(employee.getEnfants().get(i).getDateNaissance()).getYear();
			if (age <= AGE1)
				montant += MONTANT1;
			else if (age <= AGE2)
				montant += MONTANT2;
			else if (age <= AGE3)
				montant += MONTANT3;

		}
		return montant;
	}

	private String droitsResto() {
		if (employee.getAgence().getNom().equals("Lille") || employee.getAgence().getNom().equals("Roubaix")) {
			return "Acc�s au restaurant d'entreprise";
		} else
			return "Acc�s aux tickets restaurant";
	}

	public String afficherResto() {
		return droitsResto();
	}

	public String afficherChequesNoel() {
		return Integer.toString(chequesNoel()) + " �";
	}

	public String afficherChequesVacs() {
		if (chequesVacances()) {
			return "Droit aux ch�ques vacances";
		} else {
			return "Droit aux tickets restaurant";
		}

	}

}
