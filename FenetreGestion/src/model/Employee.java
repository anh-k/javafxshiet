package model;

import java.util.Optional;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import service.Repertoire;
import javafx.scene.control.Alert.AlertType;

public class Employee extends Personne {

	private ObjectProperty<Agence> agence;
	private StringProperty dateEmbauche;
	private ObservableList<Personne> enfants;
	private EmployeesRights rights;
	private Personne selectedEnfant;

	public Employee(String nom, String prenom, String dateNaissance, String sexe) {
		super(nom, prenom, dateNaissance, sexe);
		this.agence = new SimpleObjectProperty<>();
		this.dateEmbauche = new SimpleStringProperty();
		enfants = FXCollections.observableArrayList();
		rights = new EmployeesRights(this);
	}

	public Personne getSelectedEnfant() {
		return selectedEnfant;
	}

	public void setSelectedEnfant(Personne selectedEnfant) {
		this.selectedEnfant = selectedEnfant;
	}

	public EmployeesRights getRights() {
		return rights;
	}

	public void setRights(EmployeesRights rights) {
		this.rights = rights;
	}

	public final StringProperty dateEmbaucheProperty() {
		return this.dateEmbauche;
	}

	public final String getDateEmbauche() {
		return this.dateEmbaucheProperty().get();
	}

	public final void setDateEmbauche(final String dateEmbauche) {
		this.dateEmbaucheProperty().set(dateEmbauche);
	}

	public ObservableList<Personne> getEnfants() {
		return enfants;
	}

	public final ObjectProperty<Agence> agenceProperty() {
		return this.agence;
	}

	public final Agence getAgence() {
		return this.agenceProperty().get();
	}

	public final void setAgence(final Agence agence) {
		this.agenceProperty().set(agence);
	}

	public void supprEnfant() {
		if (selectedEnfant != null)
			enfants.remove(selectedEnfant);

	}

}
