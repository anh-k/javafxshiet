package dao;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
import model.Agence;
import model.Employee;
import model.Personne;

public class ContactsDAO {

	private FichierTxt fichierTxt;
	private Employee employee;
	private ArrayList<Employee> employees;

	public ContactsDAO(File nom) {

		fichierTxt = new FichierTxt(nom);

	}

	public List<Employee> lire() {
		employees = new ArrayList<Employee>();
		List<String> lignes = fichierTxt.lire();

		for (String ligne : lignes) {

			stringToEmployee(ligne);
		}
		return employees;
	}

	private void stringToEmployee(String ligne) {

		String[] parse = ligne.split("\\|");
		String head = parse[0];

		if (parse[1].equals("2")) {

			parse[1] = "F�minin";
		} else {
			if (parse[1].equals("1")) {

				parse[1] = "Masculin";
			}
		}

		if (head.equals("Salari�")) {
			employee = new Employee(parse[4], parse[2], parse[3], parse[1]);
			employee.setAgence(new Agence(parse[5]));
			employee.setDateEmbauche(parse[6]);
			employees.add(employee);
		} else if (head.equals("Enfant")) {

			employee.getEnfants().add(new Personne(employee.getNom(), parse[2], parse[3], parse[1]));
		}

	}

	public void sauver(ObservableList<Employee> allContacts) {
		List<String> lignes = new ArrayList<>();
		for (Employee employee : allContacts) {
			lignes.add(toCSV(employee));
		}
		fichierTxt.ecrire(lignes);
	}

	private String toCSV(Employee employee) {

		return employee.getNom() + "|" + employee.getPrenom() + "|" + employee.getDateNaissance() + "|"
				+ employee.getSexe();
	}

}
