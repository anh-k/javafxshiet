package dao;

import java.util.ArrayList;

public class ParametreBean {

	private String dernierRepertoire;
	private ParametersDAO parametersDAO;
	private ArrayList<String> recentFiles;

	public ParametreBean() {
		parametersDAO = new ParametersDAO();
		dernierRepertoire = parametersDAO.getRepertoire();
		recentFiles = parametersDAO.getRecentFiles();
	}

	public String getDernierRepertoire() {
		return dernierRepertoire;
	}

	public void setLastFolder(String lastFolder) {
		int pos = lastFolder.lastIndexOf('\\');
		this.dernierRepertoire = dernierRepertoire.substring(0, pos + 1);

	}

	public ArrayList<String> getRecentFiles() {
		return recentFiles;
	}

	public void save() {

		parametersDAO.save(this);
	}

}
