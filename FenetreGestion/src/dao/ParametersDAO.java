package dao;

import java.io.File;
import java.util.ArrayList;

public class ParametersDAO {
	private String repertoire;
	private ArrayList<String> recentFiles;

	public ParametersDAO() {

		recentFiles = new ArrayList<>();
		String home = System.getProperty("user.home");
		FichierTxt parametreFile = new FichierTxt(new File(home + "\\monApplication.set"));
		ArrayList<String> lignes = (ArrayList<String>) parametreFile.lire();

		if (!lignes.isEmpty() && lignes.get(0).startsWith("Répertoire="))
			repertoire = lignes.get(0).substring(11);
		else
			repertoire = "\\";
		for (int i = 1; i < lignes.size(); i++) {
			recentFiles.add(lignes.get(i).substring(8));
		}

	}

	public String getRepertoire() {
		return repertoire;
	}

	public ArrayList<String> getRecentFiles() {
		return recentFiles;
	}

	public void save(ParametreBean parametres) {
		String home = System.getProperty("user.home");
		FichierTxt parametreFile = new FichierTxt(new File(home + "\\monApplication.set"));
		ArrayList<String> lignes = new ArrayList<>();
		lignes.add("Répertoire=" + parametres.getDernierRepertoire());
		recentFiles = parametres.getRecentFiles();
		for (String fichier : recentFiles) {
			lignes.add("Fichier=" + fichier);
		}
		parametreFile.ecrire(lignes);
	}
}
