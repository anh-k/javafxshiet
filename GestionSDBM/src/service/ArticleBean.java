package service;

import dao.DAOFactory;
import dao.MarqueDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import metier.Article;
import metier.Continent;
import metier.Couleur;
import metier.Fabricant;
import metier.Marque;
import metier.Pays;
import metier.Type;

public class ArticleBean {

	private ObservableList<Couleur> listeCouleur;
	private ObservableList<Type> listeType;
	private ObservableList<Marque> listeMarque;
	private ObservableList<Fabricant> listeFabricant;
	private ObservableList<Continent> listeContinent;
	private ObservableList<Pays> listePays;
	private ObservableList<Article> listeArticle;
	private Article selectedArticle;

	public ArticleBean() {
		this.listeCouleur = FXCollections.observableArrayList(DAOFactory.getCouleurDAO().getAll());
		this.listeType = FXCollections.observableArrayList(DAOFactory.getTypeDAO().getAll());
		this.listeMarque = FXCollections.observableArrayList(DAOFactory.getMarqueDAO().getAll());
		this.listeFabricant = FXCollections.observableArrayList(DAOFactory.getFabricantDAO().getAll());
		this.listeContinent = FXCollections.observableArrayList(DAOFactory.getContinentDAO().getAll());
		this.listePays = FXCollections.observableArrayList(DAOFactory.getPaysDAO().getAll());
		this.listeArticle = FXCollections.observableArrayList(DAOFactory.getArticleDAO().getAll());

	}

	public ObservableList<Couleur> getListeCouleur() {
		return listeCouleur;
	}

	public void setListeCouleur(ObservableList<Couleur> listeCouleur) {
		this.listeCouleur = listeCouleur;
	}

	public ObservableList<Type> getListeType() {
		return listeType;
	}

	public void setListeType(ObservableList<Type> listeType) {
		this.listeType = listeType;
	}

	public ObservableList<Marque> getListeMarque() {
		return listeMarque;
	}

	public void setListeMarque(ObservableList<Marque> listeMarque) {
		this.listeMarque = listeMarque;
	}

	public ObservableList<Fabricant> getListeFabricant() {
		return listeFabricant;
	}

	public void setListeFabricant(ObservableList<Fabricant> listeFabricant) {
		this.listeFabricant = listeFabricant;
	}

	public ObservableList<Continent> getListeContinent() {
		return listeContinent;
	}

	public void setListeContinent(ObservableList<Continent> listeContinent) {
		this.listeContinent = listeContinent;
	}

	public ObservableList<Pays> getListePays() {
		return listePays;
	}

	public void setListePays(ObservableList<Pays> listePays) {
		this.listePays = listePays;
	}

	public ObservableList<Article> getListeArticle() {
		return listeArticle;
	}

	public void setListeArticle(ObservableList<Article> listeArticle) {
		this.listeArticle = listeArticle;
	}

	public Article getSelectedArticle() {
		return selectedArticle;
	}

	public void setSelectedArticle(Article selectedArticle) {
		this.selectedArticle = selectedArticle;
	}

}
