package vue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

import dao.DAOFactory;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import mainsdbm.MainSDBM;
import metier.Article;
import metier.Continent;
import metier.Couleur;
import metier.Fabricant;
import metier.Marque;
import metier.Pays;
import metier.Type;

import service.ArticleBean;
import service.ArticleSearch;

public class BEERController {

	private MainSDBM mainApp;
	@FXML
	private ComboBox<Couleur> couleurBox;
	@FXML
	private ComboBox<Continent> continentBox;
	@FXML
	private ComboBox<Type> typeBox;
	@FXML
	private ComboBox<Fabricant> fabricantBox;
	@FXML
	private ComboBox<Marque> marqueBox;
	@FXML
	private ComboBox<Pays> paysBox;
	@FXML
	private Button resetButton;
	@FXML
	private RadioButton volumeTrenteTroisButton;
	@FXML
	private RadioButton volumeSoixanteQuinzeButton;
	@FXML
	private RadioButton volumeTousButton;
	@FXML
	private TableView<Article> articleTable;
	@FXML
	private TableColumn<Article, String> nomColumn;
	@FXML
	private TableColumn<Article, Integer> volumeColumn;
	@FXML
	private TableColumn<Article, Float> titrageColumn;
	@FXML
	private TableColumn<Article, Float> prixColumn;
	@FXML
	private TableColumn<Article, String> typeColumn;
	@FXML
	private TableColumn<Article, String> couleurColumn;
	@FXML
	private TableColumn<Article, String> marqueColumn;
	@FXML
	private TableColumn<Article, String> fabricantColumn;
	@FXML
	private TableColumn<Article, String> continentColumn;
	@FXML
	private TableColumn<Article, String> paysColumn;
	@FXML
	private TextField searchField;
	@FXML
	private Label detailNom;
	@FXML
	private Label detailCouleur;
	@FXML
	private Label detailMarque;
	@FXML
	private Label detailPays;
	@FXML
	private Label detailTitrage;
	@FXML
	private Label detailType;
	@FXML
	private Label detailPrix;
	@FXML
	private Label detailFabricant;
	@FXML
	private Slider minSlider;
	@FXML
	private Slider maxSlider;
	@FXML
	private Button editButton;
	@FXML
	private Button deleteButton;
	@FXML
	private Button addButton;
	@FXML
	private Label minValue;
	@FXML
	private Label maxValue;
	@FXML
	private Label nbArticlesLabel;
	@FXML
	private Label detailID;
	private ArticleBean articleBean;

	public void setMainApp(MainSDBM mainApp) {
		this.mainApp = mainApp;
		articleBean = this.mainApp.getArticleBean();
	}

	@FXML
	private void initialize() {

		articleBean = new ArticleBean();
		setTitrageIndicators();
		enableDisableButtons(true);
		initColumns();
		initComboBoxes();

		// Add Listeners

		articleTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> afficherDetailArticle((Article) newValue));

		// Set table view and other components

		articleTable.setItems(articleBean.getListeArticle());
		resetButton.setOnAction(e -> resetFilters());
		volumeTousButton.setSelected(true);
		nbArticlesLabel.setText(String.valueOf(DAOFactory.getArticleDAO().getAll().size()) + " articles");
	}

	private void enableDisableButtons(boolean correct) {

		editButton.setDisable(correct);
		deleteButton.setDisable(correct);
	}

	private void initColumns() {
		nomColumn
				.setCellValueFactory((CellDataFeatures<Article, String> feature) -> feature.getValue().getNomArticle());
		volumeColumn.setCellValueFactory(
				(CellDataFeatures<Article, Integer> feature) -> feature.getValue().getVolume().asObject());
		titrageColumn.setCellValueFactory(
				(CellDataFeatures<Article, Float> feature) -> feature.getValue().getTitrage().asObject());
		prixColumn.setCellValueFactory(
				(CellDataFeatures<Article, Float> feature) -> feature.getValue().getPrixAchat().asObject());
		typeColumn.setCellValueFactory(
				(CellDataFeatures<Article, String> feature) -> feature.getValue().getType().getNomProperty());
		couleurColumn.setCellValueFactory(
				(CellDataFeatures<Article, String> feature) -> feature.getValue().getCouleur().getNomProperty());
		marqueColumn.setCellValueFactory(
				(CellDataFeatures<Article, String> feature) -> feature.getValue().getMarque().getNomProperty());
		fabricantColumn.setCellValueFactory((CellDataFeatures<Article, String> feature) -> feature.getValue()
				.getMarque().getFabricant().getNomProperty());
		continentColumn.setCellValueFactory((CellDataFeatures<Article, String> feature) -> feature.getValue()
				.getMarque().getPays().getContinent().getNomProperty());
		paysColumn.setCellValueFactory((CellDataFeatures<Article, String> feature) -> feature.getValue().getMarque()
				.getPays().getNomProperty());
	}

	private void initComboBoxes() {

		// Couleur
		ObservableList<Couleur> listCouleur = FXCollections.observableArrayList(new Couleur(0, "Choisir une couleur"));
		listCouleur.addAll(DAOFactory.getCouleurDAO().getAll());
		couleurBox.setItems(listCouleur);

		// Continent
		ObservableList<Continent> listContinent = FXCollections
				.observableArrayList(new Continent(0, "Choisir un continent"));
		listContinent.addAll(DAOFactory.getContinentDAO().getAll());
		continentBox.setItems(listContinent);
		continentBox.valueProperty().addListener(e -> filtrerContinent());

		// Type
		ObservableList<Type> listType = FXCollections.observableArrayList(new Type(0, "Choisir un type"));
		listType.addAll(DAOFactory.getTypeDAO().getAll());
		typeBox.setItems(listType);

		// Fabricant
		ObservableList<Fabricant> listFabricant = FXCollections
				.observableArrayList(new Fabricant(0, "Choisir un fabricant"));
		listFabricant.addAll(DAOFactory.getFabricantDAO().getAll());
		fabricantBox.setItems(listFabricant);
		fabricantBox.valueProperty().addListener(e -> filtrerFabricant());

		// Marque
		ObservableList<Marque> listMarque = FXCollections.observableArrayList(new Marque(0, "Choisir une marque"));
		listMarque.addAll(DAOFactory.getMarqueDAO().getAll());
		marqueBox.setItems(listMarque);

		// Pays
		ObservableList<Pays> listPays = FXCollections.observableArrayList(new Pays(0, "Choisir un pays"));
		listPays.addAll(DAOFactory.getPaysDAO().getAll());
		paysBox.setItems(listPays);
	}

	private void filtrerContinent() {

		if (continentBox.getSelectionModel().getSelectedItem() != null
				&& (continentBox.getSelectionModel().getSelectedItem()).getIdContinent() != 0) {
			paysBox.setItems(FXCollections
					.observableArrayList((continentBox.getSelectionModel().getSelectedItem()).getListePays()));
			paysBox.getItems().add(0, new Pays());

		} else {

			ObservableList<Pays> listPays = FXCollections.observableArrayList(new Pays(0, "Choisir un pays"));
			listPays.addAll(DAOFactory.getPaysDAO().getAll());
			paysBox.setItems(listPays);

		}
	}

	private void filtrerFabricant() {

		if (fabricantBox.getSelectionModel().getSelectedItem() != null
				&& (fabricantBox.getSelectionModel().getSelectedItem()).getIdFabricant() != 0) {
			marqueBox.setItems(FXCollections
					.observableArrayList((fabricantBox.getSelectionModel().getSelectedItem()).getListeMarque()));
			marqueBox.getItems().add(0, new Marque());

		} else {

			ObservableList<Marque> listMarque = FXCollections.observableArrayList(new Marque(0, "Choisir une marque"));
			listMarque.addAll(DAOFactory.getMarqueDAO().getAll());
			marqueBox.setItems(listMarque);

		}
	}

	public void filtrerArticle() {
		setTitrageIndicators();
		ArticleSearch articleSearch = new ArticleSearch();
		articleSearch.setTitrageMin((int) minSlider.getValue());
		articleSearch.setTitrageMax((int) maxSlider.getValue());
		setVolumeButtons(articleSearch);
		articleSearch.setNomArticle(searchField.getText());
		setComboBoxes(articleSearch);
		nbArticlesLabel.setText(String.valueOf(DAOFactory.getArticleDAO().getLike(articleSearch).size()) + " articles");

		articleTable.setItems(FXCollections.observableArrayList(DAOFactory.getArticleDAO().getLike(articleSearch)));
	}

	private void setTitrageIndicators() {

		minValue.setText(String.valueOf((int) minSlider.getValue()));
		maxValue.setText(String.valueOf((int) maxSlider.getValue()));
	}

	public void verifMaxSlider() {
		if (maxSlider.getValue() <= minSlider.getValue()) {
			minSlider.setValue(maxSlider.getValue());
		}
		filtrerArticle();
	}

	public void verifMinSlider() {
		if (minSlider.getValue() >= maxSlider.getValue()) {

			maxSlider.setValue(minSlider.getValue());
		}
		filtrerArticle();
	}

	private void setVolumeButtons(ArticleSearch articleSearch) {
		if (volumeTousButton.isSelected())
			articleSearch.setVolume(0);
		else if (volumeSoixanteQuinzeButton.isSelected())
			articleSearch.setVolume(75);
		else if (volumeTrenteTroisButton.isSelected())
			articleSearch.setVolume(33);

	}

	private void setComboBoxes(ArticleSearch articleSearch) {

		articleSearch.setContinent((Continent) continentBox.getSelectionModel().getSelectedItem());
		articleSearch.setCouleur((Couleur) couleurBox.getSelectionModel().getSelectedItem());
		articleSearch.setFabricant((Fabricant) fabricantBox.getSelectionModel().getSelectedItem());
		articleSearch.setMarque((Marque) marqueBox.getSelectionModel().getSelectedItem());
		articleSearch.setPays((Pays) paysBox.getSelectionModel().getSelectedItem());
		articleSearch.setType((Type) typeBox.getSelectionModel().getSelectedItem());

	}

	public void resetFilters() {
		searchField.setText("");
		couleurBox.getSelectionModel().select(0);
		continentBox.getSelectionModel().select(0);
		typeBox.getSelectionModel().select(0);
		fabricantBox.getSelectionModel().select(0);
		marqueBox.getSelectionModel().select(0);
		paysBox.getSelectionModel().select(0);
		volumeTousButton.setSelected(true);
		minSlider.setValue(0);
		maxSlider.setValue(26);
		articleTable.setItems(articleBean.getListeArticle());
		nbArticlesLabel.setText(String.valueOf(DAOFactory.getArticleDAO().getAll().size()) + " articles");

	}

	@FXML
	private void afficherDetailArticle(Article article) {

		articleBean.setSelectedArticle(article);

		if (article != null) {

			detailID.setText(String.valueOf(article.getId().get()));
			detailNom.setText(article.getNomArticle().get());
			detailType.setText(article.getType().getNomType());
			detailCouleur.setText(article.getCouleur().getNomCouleur());
			detailMarque.setText(article.getMarque().getNomMarque());
			detailPays.setText(article.getMarque().getPays().getNomPays());
			detailTitrage.setText(article.getTitrageString());
			detailPrix.setText(article.getPrixString());
			detailFabricant.setText(article.getMarque().getFabricant().getNomFabricant());
			enableDisableButtons(false);

		} else {

			detailID.setText("");
			detailNom.setText("");
			detailType.setText("");
			detailCouleur.setText("");
			detailMarque.setText("");
			detailPays.setText("");
			detailTitrage.setText("");
			detailPrix.setText("");
			detailFabricant.setText("");
			enableDisableButtons(true);
		}
	}

	@FXML
	private void modifier() {
		mainApp.showModifBeer(articleBean);
	}

	@FXML
	private void ajouter() {
		Article newArticle = new Article(0, "", 0, 0, 0);
		newArticle.setMarque(new Marque(0, "", new Fabricant(0, ""), new Pays(0, "", new Continent(0, ""))));
		articleBean.setSelectedArticle(newArticle);

		mainApp.showModifBeer(articleBean);
		articleTable.getSelectionModel().select(null);
	}

	@FXML
	private void supprimer() {

		try {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Supprimer un article");
			alert.setContentText(
					"Voulez-vous supprimer l'article \"" + articleBean.getSelectedArticle().toString() + "\" ?");
			Optional<ButtonType> result = alert.showAndWait();

			if (result.isPresent() && result.get() == ButtonType.OK)
				DAOFactory.getArticleDAO().delete(articleBean.getSelectedArticle());

			articleTable.getSelectionModel().select(null);
			resetFilters();

		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
