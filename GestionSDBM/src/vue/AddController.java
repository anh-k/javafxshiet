package vue;

import dao.DAOFactory;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import mainsdbm.MainSDBM;
import metier.Article;
import metier.Couleur;
import metier.Marque;
import metier.Type;
import service.ArticleBean;

public class AddController {

	@FXML
	private ComboBox<Type> typeBox;

	@FXML
	private ComboBox<Couleur> couleurBox;

	@FXML
	private ComboBox<Marque> marqueBox;

	@FXML
	private TextField nomField;

	@FXML
	private TextField titrageField;

	@FXML
	private RadioButton volumeTrenteTrois;

	@FXML
	private RadioButton volumeSoixanteQuinze;

	@FXML
	private TextField prixField;

	@FXML
	private Label detailFabricant;

	@FXML
	private Label detailContinent;

	@FXML
	private Label detailPays;

	@FXML
	private Button saveButton;

	@FXML
	private Button cancelButton;

	private boolean okClicked;

	private Stage dialogStage;
	private ArticleBean articleBean;
	private Article article;
	private MainSDBM mainApp;

	@FXML
	private void initialize() {

		// Couleur
		ObservableList<Couleur> listCouleur = FXCollections.observableArrayList(new Couleur());
		listCouleur.addAll(DAOFactory.getCouleurDAO().getAll());
		couleurBox.setItems(listCouleur);

		// Marque
		ObservableList<Marque> listMarque = FXCollections.observableArrayList(new Marque());
		listMarque.addAll(DAOFactory.getMarqueDAO().getAll());
		marqueBox.setItems(listMarque);

		// Type
		ObservableList<Type> listType = FXCollections.observableArrayList(new Type());
		listType.addAll(DAOFactory.getTypeDAO().getAll());
		typeBox.setItems(listType);
	}

	@FXML
	public void checkFeatures() {
		if (marqueBox.getSelectionModel().getSelectedIndex() != 0) {
			detailFabricant.setText(marqueBox.getSelectionModel().getSelectedItem().getFabricant().getNomFabricant());
			detailContinent.setText(
					marqueBox.getSelectionModel().getSelectedItem().getPays().getContinent().getNomContinent());
			detailPays.setText(marqueBox.getSelectionModel().getSelectedItem().getPays().getNomPays());
		} else {
			detailFabricant.setText("");
			detailContinent.setText("");
			detailPays.setText("");
		}

	}

	public void setDialogStage(Stage dialogStage) {

		this.dialogStage = dialogStage;
		okClicked = false;
	}

	public void setArticle(Article article) {

		this.article = article;
		nomField.setText(article.toString());
		titrageField.setText(article.getTitrageString());
		prixField.setText(article.getPrixString());
		typeBox.getSelectionModel().select(article.getType());
		couleurBox.getSelectionModel().select(article.getCouleur());
		marqueBox.getSelectionModel().select(article.getMarque());

		if (article.getMarque().getFabricant().getNomFabricant() != null)
			detailFabricant.setText(article.getMarque().getFabricant().getNomFabricant());
		else
			detailFabricant.setText("");

		if (article.getMarque().getPays().getContinent().getNomContinent() != null)
			detailContinent.setText(article.getMarque().getPays().getContinent().getNomContinent());
		else
			detailContinent.setText("");

		if (article.getMarque().getPays().getNomPays() != null)
			detailPays.setText(article.getMarque().getPays().getNomPays());
		else
			detailPays.setText("");

		if (article.getVolume().get() == 33)
			volumeTrenteTrois.setSelected(true);
		else
			volumeSoixanteQuinze.setSelected(true);
	}

	public void setMainApp(MainSDBM mainApp) {
		this.mainApp = mainApp;
		articleBean = this.mainApp.getArticleBean();

	}

	@FXML
	void annuler() {
		dialogStage.close();
		okClicked = false;
	}

	@FXML
	void enregister() {
		if (nomField.getText().trim().equals("") || titrageField.getText().trim().equals("0.0")
				|| prixField.getText().trim().equals("0.0") || marqueBox.getSelectionModel().getSelectedIndex() == 0
				|| marqueBox.getSelectionModel().isSelected(0)) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur de saisie");
			alert.setContentText("Veuillez compléter l'ensemble des champs de saisie");
			alert.showAndWait();
		} else {

			okClicked = !article.getNomArticle().get().equals(nomField.getText())
					|| !article.getTitrage().equals(titrageField.getText())
					|| !article.getPrixAchat().equals(titrageField.getText());

			article.setNomArticle(nomField.getText());
			article.setTitrage(Float.parseFloat(titrageField.getText()));
			article.setPrixAchat(Float.parseFloat(prixField.getText()));
			article.setType((Type) typeBox.getSelectionModel().getSelectedItem());
			article.setCouleur((Couleur) couleurBox.getSelectionModel().getSelectedItem());
			article.setMarque((Marque) marqueBox.getSelectionModel().getSelectedItem());
			setArticleVolume();

			if (article.getId().get() == 0)
				DAOFactory.getArticleDAO().insert(article);
			else
				DAOFactory.getArticleDAO().update(article);
			dialogStage.close();
		}

	}

	private void setArticleVolume() {
		if (volumeTrenteTrois.isSelected())
			article.setVolume(33);
		else
			article.setVolume(75);
	}

	public boolean isOkClicked() {
		return okClicked;
	}
}
