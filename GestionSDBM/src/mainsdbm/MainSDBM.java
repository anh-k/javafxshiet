package mainsdbm;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.Article;
import service.ArticleBean;
import vue.AddController;
import vue.BEERController;
import vue.AddController;;

public class MainSDBM extends Application {

	private Stage stage;
	private BorderPane mainLayout;
	private ArticleBean articleBean;

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage stage) throws Exception {

		articleBean = new ArticleBean();
		this.stage = stage;
		stage.getIcons().add(new Image(getClass().getResourceAsStream("/resources/pint.png")));
		stage.setTitle("Gestion SDBM");
		showMainWindow();

	}

	private void showMainWindow() {
		try {

			// Init FXML
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainSDBM.class.getResource("../vue/Beer.fxml"));
			mainLayout = loader.load();

			// Create a scene
			Scene scene = new Scene(mainLayout);
			stage.setScene(scene);
			stage.setMinHeight(820);
			stage.setMinWidth(1260);

			// Init CSS
			scene.getStylesheets().add(getClass().getResource("./mainsdbm.css").toExternalForm());

			// Init Controller
			BEERController controller = loader.getController();
			controller.setMainApp(this);

			// Display stage
			stage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public boolean showModifBeer(ArticleBean articleBean) {
		try {

			// Load the fxml file
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainSDBM.class.getResource("../vue/Add.fxml"));
			AnchorPane anchorPane = loader.load();

			// Creating the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Modifier un article de Gestion SDBM");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(stage);
			Scene scene = new Scene(anchorPane);
			dialogStage.setScene(scene);
			dialogStage.getIcons().add(new Image(getClass().getResourceAsStream("/resources/pint.png")));
			dialogStage.setResizable(false);

			// sending parameters to the controller
			AddController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setMainApp(this);
			controller.setArticle(articleBean.getSelectedArticle());

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();
			return controller.isOkClicked();
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
	}

	public ArticleBean getArticleBean() {
		return articleBean;
	}

	public void setArticleBean(ArticleBean articleBean) {
		this.articleBean = articleBean;
	}

}
