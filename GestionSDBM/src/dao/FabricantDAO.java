package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Continent;
import metier.Couleur;
import metier.Fabricant;
import metier.Marque;
import metier.Type;

public class FabricantDAO extends DAO<Fabricant> {

	public FabricantDAO(Connection connexion) {
		super(connexion);

	}

	@Override
	public Fabricant getById(int id) {
		ResultSet rs;
		Fabricant fabricant = new Fabricant();
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "select ID_FABRICANT, NOM_FABRICANT from FABRICANT where ID_FABRICANT = " + id;
			rs = stmt.executeQuery(strCmd);

			fabricant = new Fabricant(rs.getInt(1), rs.getString(2));
			isOk = true;
		} catch (Exception e) {
			e.printStackTrace();

		}
		if (isOk) {

			return fabricant;

		} else
			return null;
	}

	@Override
	public ArrayList<Fabricant> getAll() {
		ResultSet rs;
		ArrayList<Fabricant> liste = new ArrayList<Fabricant>();

		try {
			Statement stmt = connexion.createStatement();

			// Set column

			String strCmd = "select FABRICANT.ID_FABRICANT, nom_fabricant,id_marque, nom_marque from FABRICANT "
					+ "join MARQUE on MARQUE.ID_FABRICANT = FABRICANT.ID_FABRICANT "
					+ "order by NOM_FABRICANT,NOM_MARQUE";
			rs = stmt.executeQuery(strCmd);

			Fabricant fabricantLu = new Fabricant();
			while (rs.next()) {
				if (fabricantLu.getIdFabricant() != rs.getInt(1)) {
					fabricantLu = new Fabricant(rs.getInt(1), rs.getString(2));
					liste.add(fabricantLu);
				}

				fabricantLu.getListeMarque().add(new Marque(rs.getInt(3), rs.getString(4)));
			}

			rs.close();

		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return liste;
	}

	@Override
	public boolean insert(Fabricant fabricant) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "insert into Fabricant (nom_Fabricant) values ('" + fabricant.toString() + "')";
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean update(Fabricant fabricant) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "update Fabricant set nom_Fabricant = " + fabricant.getNomFabricant()
					+ " where id_Fabricant =" + fabricant.getIdFabricant();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean delete(Fabricant fabricant) {

		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "delete from Fabricant where id_Fabricant =" + fabricant.getIdFabricant();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}
}