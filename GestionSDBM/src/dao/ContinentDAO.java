package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Continent;
import metier.Couleur;
import metier.Pays;

public class ContinentDAO extends DAO<Continent> {

	public ContinentDAO(Connection connexion) {
		super(connexion);

	}

	@Override
	public Continent getById(int id) {
		ResultSet rs;
		Continent continent = new Continent();
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "select ID_Continent, NOM_Continent from Continent where ID_Continent = " + id;
			rs = stmt.executeQuery(strCmd);

			continent = new Continent(rs.getInt(1), rs.getString(2));
			isOk = true;
		} catch (Exception e) {
			e.printStackTrace();

		}
		if (isOk) {

			return continent;

		} else
			return null;
	}

	@Override
	public ArrayList<Continent> getAll() {
		ResultSet rs;
		ArrayList<Continent> liste = new ArrayList<Continent>();

		try {
			Statement stmt = connexion.createStatement();

			// Set column

			String strCmd = 
					"select CONTINENT.id_continent,nom_continent,id_pays, nom_pays from CONTINENT "
					+ "join PAYS on PAYS.ID_CONTINENT = CONTINENT.ID_CONTINENT " 
					+ "order by NOM_CONTINENT,NOM_PAYS";
			rs = stmt.executeQuery(strCmd);

			Continent continentLu = new Continent();
			while (rs.next()) {
				if (continentLu.getIdContinent() != rs.getInt(1)) {
					continentLu = new Continent(rs.getInt(1), rs.getString(2));
					liste.add(continentLu);
				}
				continentLu.getListePays().add(new Pays(rs.getInt(3), rs.getString(4)));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return liste;
	}

	@Override
	public boolean insert(Continent continent) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "insert into continent (nom_continent) values ('" + continent.toString() + "')";
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean update(Continent continent) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "update Continent set nom_continent = " + continent.getNomContinent()
					+ " where id_continent =" + continent.getIdContinent();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean delete(Continent continent) {

		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "delete from Continent where id_continent =" + continent.getIdContinent();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}
}