package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import metier.Article;
import metier.Continent;
import metier.Couleur;
import metier.Fabricant;
import metier.Marque;
import metier.Pays;
import metier.Type;
import service.ArticleSearch;

public class ArticleDAO extends DAO<Article> {

	public ArticleDAO(Connection connexion) {
		super(connexion);

	}

	@Override
	public Article getById(int id) {
		ResultSet rs;
		Article article = new Article(0, null);
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "select ID_Article, NOM_Article from Article where ID_Article = " + id;
			rs = stmt.executeQuery(strCmd);

			article = new Article(rs.getInt(1), rs.getString(2));
			isOk = true;
		} catch (Exception e) {
			e.printStackTrace();

		}
		if (isOk) {

			return article;

		} else
			return null;
	}

	public ArrayList<Article> getLike(ArticleSearch articleSearch) {
		ResultSet rs;
		ArrayList<Article> liste = new ArrayList<Article>();
		Article article;
		String procedureStockee = "{call sp_ArticlesQBE (?,?,?,?,?,?,?,?,?,?)}";
		try (CallableStatement cStmt = this.connexion.prepareCall(procedureStockee)) {

			cStmt.setString(1, articleSearch.getNomArticle());

			if (articleSearch.getCouleur() == null || articleSearch.getCouleur().getIdCouleur() == 0)
				cStmt.setNull(2, Types.INTEGER);
			else
				cStmt.setInt(2, articleSearch.getCouleur().getIdCouleur());

			if (articleSearch.getPays() == null || articleSearch.getPays().getIdPays() == 0)
				cStmt.setNull(3, Types.INTEGER);
			else
				cStmt.setInt(3, articleSearch.getPays().getIdPays());

			if (articleSearch.getContinent() == null || articleSearch.getContinent().getIdContinent() == 0)
				cStmt.setNull(4, Types.INTEGER);
			else
				cStmt.setInt(4, articleSearch.getContinent().getIdContinent());

			if (articleSearch.getVolume() == 0) {
				cStmt.setNull(5, Types.INTEGER);
			} else
				cStmt.setInt(5, articleSearch.getVolume());

			if (articleSearch.getFabricant() == null || articleSearch.getFabricant().getIdFabricant() == 0)
				cStmt.setNull(6, Types.INTEGER);
			else
				cStmt.setInt(6, articleSearch.getFabricant().getIdFabricant());

			if (articleSearch.getType() == null || articleSearch.getType().getIdType() == 0)
				cStmt.setNull(7, Types.INTEGER);
			else
				cStmt.setInt(7, articleSearch.getType().getIdType());

			if (articleSearch.getMarque() == null || articleSearch.getMarque().getIdMarque() == 0)
				cStmt.setNull(8, Types.INTEGER);
			else
				cStmt.setInt(8, articleSearch.getMarque().getIdMarque());

			cStmt.setInt(9, articleSearch.getTitrageMin());
			cStmt.setInt(10, articleSearch.getTitrageMax());

			cStmt.execute();
			rs = cStmt.getResultSet();

			while (rs.next()) {
				article = new Article(rs.getInt(11), rs.getString(12), rs.getInt(13), rs.getFloat(17), rs.getFloat(16));
				article.setCouleur(new Couleur(rs.getInt(5), rs.getString(6)));
				article.setType(new Type(rs.getInt(1), rs.getString(2)));

				article.setMarque(new Marque(rs.getInt(7), rs.getString(8),
						new Fabricant(rs.getInt(14), rs.getString(15)),
						new Pays(rs.getInt(9), rs.getString(10), new Continent(rs.getInt(3), rs.getString(4)))));
				liste.add(article);

			}

			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return liste;
	}

	@Override
	public ArrayList<Article> getAll() {
		ResultSet rs;
		ArrayList<Article> liste = new ArrayList<Article>();
		Article article;

		try {
			Statement stmt = connexion.createStatement();

			// Set column

			String strCmd = "select * from Vue_Article";
			rs = stmt.executeQuery(strCmd);

			while (rs.next()) {

				article = new Article(rs.getInt(11), rs.getString(12), rs.getInt(13), rs.getFloat(17), rs.getFloat(16));
				article.setCouleur(new Couleur(rs.getInt(5), rs.getString(6)));
				article.setType(new Type(rs.getInt(1), rs.getString(2)));

				article.setMarque(new Marque(rs.getInt(7), rs.getString(8),
						new Fabricant(rs.getInt(14), rs.getString(15)),
						new Pays(rs.getInt(9), rs.getString(10), new Continent(rs.getInt(3), rs.getString(4)))));
				liste.add(article);

			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return liste;
	}

	@Override
	public boolean insert(Article article) {
		try {

			String sql = "insert into Article (NOM_ARTICLE, TITRAGE, PRIX_ACHAT, ID_TYPE, ID_COULEUR,ID_MARQUE,VOLUME) values (?,?,?,?,?,?,?)";
			PreparedStatement ps = connexion.prepareStatement(sql);
			ps.setString(1, article.getNomArticle().get());
			ps.setFloat(2, article.getTitrage().get());
			ps.setFloat(3, article.getPrixAchat().get());

			if (article.getType() == null || article.getType().getIdType() == 0)
				ps.setNull(4, Types.INTEGER);
			else

				ps.setInt(4, article.getType().getIdType());

			if (article.getCouleur() == null || article.getCouleur().getIdCouleur() == 0)
				ps.setNull(5, Types.INTEGER);
			else
				ps.setInt(5, article.getCouleur().getIdCouleur());

			ps.setInt(6, article.getMarque().getIdMarque());
			ps.setInt(7, article.getVolume().get());

			ps.execute();
			ps.close();
			return true;

		} catch (Exception e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Erreur dans les champs de saisie");
			alert.setContentText("Probl�me rencontr� lors de la saisie, veuillez r�essayer.");
			alert.showAndWait();
			return false;
		}
	}

	@Override
	public boolean update(Article article) {

		try {

			String sql = "UPDATE ARTICLE SET NOM_ARTICLE = ?, TITRAGE = round(?,2) , PRIX_ACHAT= round(?,2) , ID_TYPE = ?, ID_COULEUR = ?, ID_MARQUE = ?, VOLUME = ? WHERE ID_ARTICLE = ?";
			PreparedStatement ps = connexion.prepareStatement(sql);

			ps.setString(1, article.getNomArticle().get());
			ps.setFloat(2, article.getTitrage().get());
			ps.setFloat(3, article.getPrixAchat().get());

			if (article.getType() == null || article.getType().getIdType() == 0)
				ps.setNull(4, Types.INTEGER);
			else

				ps.setInt(4, article.getType().getIdType());

			if (article.getCouleur() == null || article.getCouleur().getIdCouleur() == 0)
				ps.setNull(5, Types.INTEGER);
			else
				ps.setInt(5, article.getCouleur().getIdCouleur());

			ps.setInt(6, article.getMarque().getIdMarque());
			ps.setInt(7, article.getVolume().get());
			ps.setInt(8, article.getId().get());

			ps.execute();
			ps.close();
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean delete(Article article) {

		try {

			String sql = "DELETE FROM ARTICLE WHERE ID_ARTICLE = ?";
			PreparedStatement ps = connexion.prepareStatement(sql);

			ps.setInt(1, article.getId().get());

			ps.execute();
			ps.close();
			Alert newAlert = new Alert(AlertType.INFORMATION);
			newAlert.setTitle("Confirmation");
			newAlert.setContentText("Article supprim� !");
			newAlert.showAndWait();
			return true;

		} catch (Exception e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Supprimer un article");
			alert.setContentText("Impossible de supprimer l'article \"" + article.getNomArticle().get() + "\" !");
			alert.showAndWait();
			return false;
		}

	}
}