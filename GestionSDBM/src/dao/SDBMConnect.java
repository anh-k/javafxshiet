package dao;

import java.sql.Connection;
import java.sql.DriverManager;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

public class SDBMConnect {
	// Declare the JDBC objects.
	private static Connection connexion;

	private SDBMConnect() {
		try {
//			String dbURL = "jdbc:sqlserver://DESKTOP-FCU3LC0:1433;databaseName=SDBM";
//			String user = "javaSDBM";
//			String pass = "javaSDBM";
//			connexion = DriverManager.getConnection(dbURL, user, pass);

			SQLServerDataSource ds = new SQLServerDataSource();
			ds.setServerName("STA7400548");
			ds.setPortNumber(1433);
			ds.setDatabaseName("SDBM");
			ds.setIntegratedSecurity(false);
			ds.setUser("javasdbm");
			ds.setPassword("mdp");
			connexion = ds.getConnection();
		}

		// Handle any errors that may have occurred.
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static synchronized Connection getInstance() {
		if (connexion == null)
			new SDBMConnect();
		return connexion;
	}

}