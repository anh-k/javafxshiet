package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Continent;
import metier.Couleur;
import metier.Fabricant;
import metier.Type;

public class TypeDAO extends DAO<Type> {

	public TypeDAO(Connection connexion) {
		super(connexion);

	}

	@Override
	public Type getById(int id) {
		ResultSet rs;
		Type type = new Type();
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "select ID_TYPE, NOM_Type from TYPEBIERE where ID_Type = " + id;
			rs = stmt.executeQuery(strCmd);

			type = new Type(rs.getInt(1), rs.getString(2));
			isOk = true;
		} catch (Exception e) {
			e.printStackTrace();

		}
		if (isOk) {

			return type;

		} else
			return null;
	}

	@Override
	public ArrayList<Type> getAll() {
		ResultSet rs;
		ArrayList<Type> liste = new ArrayList<Type>();

		try {
			Statement stmt = connexion.createStatement();

			// Set column

			String strCmd = "SELECT id_Type, nom_Type from TYPEBIERE order by nom_Type";
			rs = stmt.executeQuery(strCmd);

			while (rs.next()) {

				liste.add(new Type(rs.getInt(1), rs.getString(2)));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return liste;
	}

	@Override
	public boolean insert(Type type) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "insert into TYPEBIERE (nom_type) values ('" + type.toString() + "')";
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean update(Type type) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "update TYPEBIERE set nom_type = " + type.getNomType() + " where id_type ="
					+ type.getIdType();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean delete(Type type) {

		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "delete from TYPEBIERE where id_type =" + type.getIdType();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}
}