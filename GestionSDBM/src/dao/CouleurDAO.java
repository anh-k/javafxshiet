package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Couleur;
import metier.Type;

public class CouleurDAO extends DAO<Couleur> {

	public CouleurDAO(Connection connexion) {
		super(connexion);

	}

	@Override
	public Couleur getById(int id) {
		ResultSet rs;
		Couleur couleur = new Couleur();
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "select ID_Couleur, NOM_Couleur from Couleur where ID_Couleur = " + id;
			rs = stmt.executeQuery(strCmd);

			couleur = new Couleur(rs.getInt(1), rs.getString(2));
			isOk = true;
		} catch (Exception e) {
			e.printStackTrace();

		}
		if (isOk) {

			return couleur;

		} else
			return null;
	}

	@Override
	public ArrayList<Couleur> getAll() {
		ResultSet rs;
		ArrayList<Couleur> liste = new ArrayList<Couleur>();

		try {
			Statement stmt = connexion.createStatement();

			// Set column

			String strCmd = "SELECT id_couleur, nom_couleur from couleur order by nom_couleur";
			rs = stmt.executeQuery(strCmd);

			while (rs.next()) {

				liste.add(new Couleur(rs.getInt(1), rs.getString(2)));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return liste;
	}

	@Override
	public boolean insert(Couleur couleur) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "insert into couleur (nom_couleur) values ('" + couleur.toString() + "')";
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean update(Couleur couleur) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "update Couleur set nom_couleur = " + couleur.getNomCouleur() + " where id_couleur ="
					+ couleur.getIdCouleur();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean delete(Couleur couleur) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "delete from couleur where id_couleur =" + couleur.getIdCouleur();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}
}
