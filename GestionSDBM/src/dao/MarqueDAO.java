package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Continent;
import metier.Couleur;
import metier.Fabricant;
import metier.Marque;
import metier.Pays;
import metier.Type;

public class MarqueDAO extends DAO<Marque> {

	public MarqueDAO(Connection connexion) {
		super(connexion);

	}

	@Override
	public Marque getById(int id) {
		ResultSet rs;
		Marque marque = new Marque();
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "select ID_MARQUE, NOM_MARQUE from Marque where ID_MARQUE = " + id;
			rs = stmt.executeQuery(strCmd);

			marque = new Marque(rs.getInt(1), rs.getString(2));
			isOk = true;
		} catch (Exception e) {
			e.printStackTrace();

		}
		if (isOk) {

			return marque;

		} else
			return null;
	}

	@Override
	public ArrayList<Marque> getAll() {
		ResultSet rs;
		ArrayList<Marque> liste = new ArrayList<Marque>();

		try {
			Statement stmt = connexion.createStatement();

			// Set column

			String strCmd = "select MARQUE.ID_MARQUE, nom_marque, MARQUE.ID_fabricant, nom_fabricant, MARQUE.ID_PAYS, NOM_PAYS, PAYS.ID_CONTINENT, NOM_CONTINENT from MARQUE "
					+ "join FABRICANT on FABRICANT.ID_FABRICANT = MARQUE.ID_FABRICANT "
					+ "join PAYS on PAYS.ID_PAYS = MARQUE.ID_PAYS "
					+ "join CONTINENT on CONTINENT.ID_CONTINENT = PAYS.ID_CONTINENT "
					+ "order by NOM_MARQUE,NOM_FABRICANT";
			rs = stmt.executeQuery(strCmd);

			while (rs.next()) {

				liste.add(new Marque(rs.getInt(1), rs.getString(2), new Fabricant(rs.getInt(3), rs.getString(4)),
						new Pays(rs.getInt(5), rs.getString(6), new Continent(rs.getInt(7), rs.getString(8)))));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return liste;
	}

	@Override
	public boolean insert(Marque marque) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "insert into Marque (nom_Marque) values ('" + marque.toString() + "')";
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean update(Marque marque) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "update marque set nom_marque = " + marque.getNomMarque() + " where id_marque ="
					+ marque.getIdMarque();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean delete(Marque marque) {

		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "delete from marque where id_marque =" + marque.getIdMarque();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}
}