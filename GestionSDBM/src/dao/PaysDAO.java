package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import metier.Continent;
import metier.Pays;

public class PaysDAO extends DAO<Pays> {

	public PaysDAO(Connection connexion) {
		super(connexion);

	}

	@Override
	public Pays getById(int id) {
		ResultSet rs;
		Pays pays = new Pays();
		boolean isOk = false;

		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "select ID_Pays, NOM_Pays from Pays where ID_Pays = " + id;
			rs = stmt.executeQuery(strCmd);

			pays = new Pays(rs.getInt(1), rs.getString(2));
			isOk = true;
		} catch (Exception e) {
			e.printStackTrace();

		}
		if (isOk) {

			return pays;

		} else
			return null;
	}

	@Override
	public ArrayList<Pays> getAll() {
		ResultSet rs;
		ArrayList<Pays> liste = new ArrayList<Pays>();

		try {
			Statement stmt = connexion.createStatement();

			// Set column

			String strCmd = "select PAYS.ID_PAYS,nom_pays, PAYS.ID_CONTINENT,nom_continent from PAYS "
					+ "join CONTINENT on CONTINENT.ID_CONTINENT= PAYS.ID_CONTINENT "
					+ "order by NOM_PAYS,NOM_CONTINENT";
			rs = stmt.executeQuery(strCmd);

			while (rs.next()) {

				liste.add(new Pays(rs.getInt(1), rs.getString(2), new Continent(rs.getInt(3), rs.getString(4))));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return liste;
	}

	@Override
	public boolean insert(Pays pays) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "insert into Pays (nom_Pays) values ('" + pays.toString() + "')";
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean update(Pays pays) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "update Pays set nom_Pays = " + pays.getNomPays() + " where id_Pays =" + pays.getIdPays();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean delete(Pays pays) {
		try {
			Statement stmt = connexion.createStatement();

			// Set column
			String strCmd = "delete from Pays where id_Pays =" + pays.getIdPays();
			stmt.execute(strCmd);
			return true;

		} catch (Exception e) {
			return false;
		}

	}
}
