package metier;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Couleur {

	private int idCouleur;
	private String nomCouleur;

	public Couleur() {

	}

	public Couleur(int idCouleur, String nomCouleur) {
		this.idCouleur = idCouleur;
		this.nomCouleur = nomCouleur;
	}

	public int getIdCouleur() {
		return idCouleur;
	}

	public void setIdCouleur(int idCouleur) {
		this.idCouleur = idCouleur;
	}

	public String getNomCouleur() {
		return nomCouleur;
	}

	public void setNomCouleur(String nomCouleur) {
		this.nomCouleur = nomCouleur;
	}

	public StringProperty getNomProperty() {
		return new SimpleStringProperty(nomCouleur);
	}

	@Override
	public String toString() {
		return nomCouleur;
	}

}
