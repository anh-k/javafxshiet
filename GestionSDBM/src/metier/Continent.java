package metier;

import java.util.ArrayList;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

public class Continent {

	private int idContinent;
	private String nomContinent;
	private ArrayList<Pays> listePays;

	public Continent() {

	}

	public Continent(int idContinent, String nomContinent) {
		this.idContinent = idContinent;
		this.nomContinent = nomContinent;
		listePays = new ArrayList<Pays>();
	}

	public int getIdContinent() {
		return idContinent;
	}

	public void setIdContinent(int idContinent) {
		this.idContinent = idContinent;
	}

	public String getNomContinent() {
		return nomContinent;
	}

	public void setNomContinent(String nomContinent) {
		this.nomContinent = nomContinent;
	}

	public ArrayList<Pays> getListePays() {
		return listePays;
	}

	public void setListePays(ArrayList<Pays> listePays) {
		this.listePays = listePays;
	}
	
	public StringProperty getNomProperty () {
		return new SimpleStringProperty (nomContinent);
	}

	@Override
	public String toString() {
		return nomContinent;
	}

}
