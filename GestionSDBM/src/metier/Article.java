package metier;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Article {

	private IntegerProperty id;
	private StringProperty nomArticle;
	private IntegerProperty volume;
	private FloatProperty titrage;
	private FloatProperty prixAchat;
	private Marque marque;
	private Couleur couleur;
	private Type type;

	public Article(int id, String nomArticle, int volume, float titrage, float prixAchat) {
		this.id = new SimpleIntegerProperty();
		this.id.set(id);
		this.nomArticle = new SimpleStringProperty();
		this.nomArticle.set(nomArticle);
		this.volume = new SimpleIntegerProperty();
		this.volume.set(volume);
		this.titrage = new SimpleFloatProperty();
		this.titrage.set(titrage);
		this.prixAchat = new SimpleFloatProperty();
		this.prixAchat.set(prixAchat);
	}

	public Article(int id, String nomArticle) {
		this.id.set(id);
		this.nomArticle.set(nomArticle);
	}

	public Article() {

	}

	public IntegerProperty getId() {
		return id;
	}

	public void setId(IntegerProperty id) {
		this.id = id;
	}

	public StringProperty getNomArticle() {
		return nomArticle;
	}

	public void setNomArticle(StringProperty nomArticle) {
		this.nomArticle = nomArticle;
	}

	public IntegerProperty getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume.set(volume);
	}

	public FloatProperty getTitrage() {
		return titrage;
	}

	public void setTitrage(final float titrage) {
		this.titrage.set(titrage);
	}

	public FloatProperty getPrixAchat() {
		return prixAchat;
	}

	public void setPrixAchat(float prixAchat) {
		this.prixAchat.set(prixAchat);
	}

	public String getPrixString() {
		return "" + prixAchat.get() + "";
	}

	public String getTitrageString() {
		return "" + titrage.get() + "";
	}

	public Marque getMarque() {
		return marque;
	}

	public void setMarque(Marque marque) {
		this.marque = marque;
	}

	public Couleur getCouleur() {
		return couleur;
	}

	public void setCouleur(Couleur couleur) {
		this.couleur = couleur;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public final StringProperty nomArticleProperty() {
		return this.nomArticle;
	}

	public final void setNomArticle(final String nomArticle) {
		this.nomArticleProperty().set(nomArticle);
	}

	@Override
	public String toString() {
		return nomArticle.get();
	}

}
