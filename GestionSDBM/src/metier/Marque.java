package metier;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Marque {

	private int idMarque;
	private String nomMarque;
	private Fabricant fabricant;
	private Pays pays;

	public Marque() {

	}

	public Marque(int idMarque, String nomMarque) {
		this.idMarque = idMarque;
		this.nomMarque = nomMarque;

	}

	public Marque(int idMarque, String nomMarque, Fabricant fabricant, Pays pays) {
		this.idMarque = idMarque;
		this.nomMarque = nomMarque;
		this.pays = pays;
		this.fabricant = fabricant;
	}

	public int getIdMarque() {
		return idMarque;
	}

	public void setIdMarque(int idMarque) {
		this.idMarque = idMarque;
	}

	public String getNomMarque() {
		return nomMarque;
	}

	public void setNomMarque(String nomMarque) {
		this.nomMarque = nomMarque;
	}

	public Fabricant getFabricant() {
		return fabricant;
	}

	public void setFabricant(Fabricant fabricant) {
		this.fabricant = fabricant;
	}

	public StringProperty getNomProperty() {
		return new SimpleStringProperty(nomMarque);
	}

	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}

	@Override
	public String toString() {
		return nomMarque;
	}
}
