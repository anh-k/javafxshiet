package metier;

import java.util.ArrayList;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Fabricant {

	private int idFabricant;
	private String nomFabricant;
	private ArrayList<Marque> listeMarque;

	public Fabricant() {

	}

	public Fabricant(int idFabricant, String nomFabricant) {
		this.idFabricant = idFabricant;
		this.nomFabricant = nomFabricant;
		listeMarque = new ArrayList<Marque>();
	}

	public int getIdFabricant() {
		return idFabricant;
	}

	public void setIdFabricant(int idFabricant) {
		this.idFabricant = idFabricant;
	}

	public String getNomFabricant() {
		return nomFabricant;
	}

	public void setNomFabricant(String nomFabricant) {
		this.nomFabricant = nomFabricant;
	}

	public ArrayList<Marque> getListeMarque() {
		return listeMarque;
	}

	public void setListeMarque(ArrayList<Marque> listeMarque) {
		this.listeMarque = listeMarque;
	}

	public StringProperty getNomProperty() {
		return new SimpleStringProperty(nomFabricant);
	}

	@Override
	public String toString() {
		return nomFabricant;
	}

}
