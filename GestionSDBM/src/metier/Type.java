package metier;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Type {

	private int idType;
	private String nomType;

	public Type() {

	}

	public Type(int idType, String nomType) {
		this.idType = idType;
		this.nomType = nomType;
	}

	public int getIdType() {
		return idType;
	}

	public void setIdType(int idType) {
		this.idType = idType;
	}

	public String getNomType() {
		return nomType;
	}

	public void setNomType(String nomType) {
		this.nomType = nomType;
	}

	public StringProperty getNomProperty() {
		return new SimpleStringProperty(nomType);
	}

	@Override
	public String toString() {
		return nomType;
	}

}
