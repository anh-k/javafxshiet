package metier;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Pays {
	private int idPays;
	private String nomPays;
	private Continent continent;

	public Pays() {

	}

	public Pays(int idPays, String nomPays) {
		this.idPays = idPays;
		this.nomPays = nomPays;

	}

	public Pays(int idPays, String nomPays, Continent continent) {
		this.idPays = idPays;
		this.nomPays = nomPays;
		this.continent = continent;

	}

	public int getIdPays() {
		return idPays;
	}

	public void setIdPays(int idPays) {
		this.idPays = idPays;
	}

	public String getNomPays() {
		return nomPays;
	}

	public void setNomPays(String nomPays) {
		this.nomPays = nomPays;
	}

	public Continent getContinent() {
		return continent;
	}

	public void setContinent(Continent continent) {
		this.continent = continent;
	}

	public StringProperty getNomProperty() {
		return new SimpleStringProperty(nomPays);
	}

	@Override
	public String toString() {
		return nomPays;
	}
}