package fr.fs.ce;

import fr.fs.salarie.Personne;
import fr.fs.salarie.Salarie;

public class Main
{
	public static void main(String[] args)
	{
		Salarie salarie = new Salarie("Dupont", "Jean", "27/06/1962", "27/06/1976");
		salarie.ajouterEnfant(new Personne("Dupont", "pierre-marie", "09/08/2010"));
		salarie.ajouterEnfant(new Personne("Dupont", "marie-pierre", "09/08/2010"));
		salarie.ajouterEnfant(new Personne("Dupont", "paul", "09/08/2002"));
		salarie.afficher();
		salarie.getEnfants().removeAt(1);
		salarie.afficher();	
	}
}
