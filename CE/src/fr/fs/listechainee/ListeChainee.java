package fr.fs.listechainee;

public class ListeChainee<T>
{
	private Maillon<T> premier;
	private Maillon<T> dernier;
	private int length;
	boolean sorted;

	public ListeChainee(boolean sorted)
	{
		premier = null;
		dernier = null;
		length = 0;
		this.sorted = sorted;
	}

	public void ajouter(T object)
	{
		length++;
		Maillon<T> newMaillon = new Maillon<>(object);
		if (premier == null)
		{
			premier = dernier = newMaillon;
			sorted = sorted && premier.getInstance() instanceof Comparable ;
		}
		else
		{
			suiteDeListe(newMaillon);
		}
	}

	public int getLength()
	{
		return length;
	}

	public T getAt(int indice)
	{
		Maillon<T> maillon = getMaillon(indice);
		return maillon.getInstance();
	}

	private Maillon<T> getMaillon(int indice)
	{
		Maillon<T> maillon = premier;
		for (int i = 0; i < indice; i++)
		{
			maillon = maillon.getSuivant();
		}
		return maillon;
	}

	public void removeAt(int indice)
	{
		Maillon<T> maillonSupp = getMaillon(indice);
		if (indice == 0)
		{
			maillonSupp.getSuivant().setPrecedent(null);
			premier = maillonSupp.getSuivant();
		}
		else if (indice == length - 1)
		{
			maillonSupp.getPrecedent().setSuivant(null);
			dernier = maillonSupp.getPrecedent();
		}
		else
		{
			maillonSupp.getPrecedent().setSuivant(maillonSupp.getSuivant());
			maillonSupp.getSuivant().setPrecedent(maillonSupp.getPrecedent());
		}
		length--;
	}

	private void suiteDeListe(Maillon<T> newMaillon)
	{
		T plusPetit = premier.getInstance();
		T plusGrand = dernier.getInstance();
		if (!sorted || ((Comparable) plusGrand).compareTo(newMaillon.getInstance()) <= 0)
			nouveauDernier(newMaillon);
		else if (((Comparable) plusPetit).compareTo(newMaillon.getInstance()) >= 0)
			nouveauPremier(newMaillon);
		else
			insererMaillon(newMaillon);
	}

	
	
	private void insererMaillon(Maillon<T> newMaillon)
	{
		Maillon<T> maillon = premier;
		T valeur;
		do
		{
			maillon = maillon.getSuivant();
			valeur = maillon.getInstance();
		}
		while (((Comparable) valeur).compareTo(newMaillon.getInstance()) < 0);
		insererAvant(maillon, newMaillon);
	}

	private void nouveauDernier(Maillon<T> newMaillon)
	{
		newMaillon.setPrecedent(dernier);
		dernier.setSuivant(newMaillon);
		dernier = newMaillon;
	}

	private void nouveauPremier(Maillon<T> newMaillon)
	{
		newMaillon.setSuivant(premier);
		premier.setPrecedent(newMaillon);
		premier = newMaillon;
	}

	private void insererAvant(Maillon<T> maillon, Maillon<T> newMaillon)
	{
		newMaillon.setSuivant(maillon);
		newMaillon.setPrecedent(maillon.getPrecedent());
		maillon.setPrecedent(newMaillon);
		newMaillon.getPrecedent().setSuivant(newMaillon);
	}

	public void afficherAsc()
	{
		System.out.print("\nListe Chain�e Ascendante  :");
		Maillon<T> maillon = premier;
		while (maillon != null)
		{
			System.out.printf(" %s", maillon.getInstance());
			maillon = maillon.getSuivant();
		}
	}

	public void afficherDesc()
	{
		System.out.print("\nListe Chain�e Descendante :");
		Maillon<T> maillon = dernier;
		while (maillon != null)
		{
			System.out.printf(" %s", maillon.getInstance());
			maillon = maillon.getPrecedent();
		}
	}

}
