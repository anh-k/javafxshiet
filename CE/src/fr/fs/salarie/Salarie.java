package fr.fs.salarie;

import java.time.LocalDate;

import fr.fs.listechainee.ListeChainee;
import fr.fs.outils.OutilsDate;

public class Salarie extends Personne
{
	private LocalDate dateEmbauche;
	private ListeChainee<Personne> enfants;
	private DroitCE droits;
	private Agence agence;
	
	public Salarie(String nom, String prenom, String dateNaissance, String dateEmbauche)
	{
		super(nom, prenom, dateNaissance);
		this.dateEmbauche = OutilsDate.stringToDate(dateEmbauche);
		enfants = new ListeChainee<>(true);
		droits = new DroitCE(this);
	}
	
	public void muter (Agence agence)
	{
		this.agence = agence;
	}

	Agence getAgence()
	{
		return agence;
	}

	LocalDate getDateEmbauche()
	{
		return dateEmbauche;
	}


	public ListeChainee<Personne> getEnfants()
	{
		return enfants;
	}

	public void ajouterEnfant(Personne enfant)
	{
		enfants.ajouter(enfant);
	}

	@Override
	public void afficher()
	{
		super.afficher();
		System.out.printf(" embauch� le : %s%n", OutilsDate.dateToLitteral(dateEmbauche));
		for (int i = 0; i < enfants.getLength(); i++)
		{
			enfants.getAt(i).afficher();
		}
		droits.afficher();
	}
}
