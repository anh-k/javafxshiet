package fr.fs.salarie;

public class Agence
{
	private String nom;
	private boolean restaurant;
	public Agence(String nom)
	{
		super();
		this.nom = nom;
	}
	
	public void ouvrirRestaurant()
	{
		restaurant = true;
	}
	public void fermerRestaurant()
	{
		restaurant = false;
	}

	boolean isRestaurant()
	{
		return restaurant;
	}

	
}
