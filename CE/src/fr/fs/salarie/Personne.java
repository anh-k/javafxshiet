package fr.fs.salarie;

import java.time.LocalDate;

import fr.fs.outils.OutilsChaine;
import fr.fs.outils.OutilsDate;

public class Personne implements Comparable<Personne>
{
	private String nom;
	private String prenom;
	private LocalDate dateNaissance;

	public Personne(String nom, String prenom, String dateNaissance)
	{
		super();
		this.nom = nom.toUpperCase();
		this.prenom = OutilsChaine.toNomPropre(prenom);
		this.dateNaissance = OutilsDate.stringToDate(dateNaissance);
	}

	public String getNom()
	{
		return nom;
	}

	public String getPrenom()
	{
		return prenom;
	}

	public LocalDate getDateNaissance()
	{
		return dateNaissance;
	}

	public void afficher()
	{
		System.out.printf("%n%s %s, n� le %s", prenom, getNom(), OutilsDate.dateToString(getDateNaissance()));
	}

	@Override
	public int compareTo(Personne other)
	{
		if (dateNaissance.compareTo(other.dateNaissance) == 0)
		{
			if (nom.compareTo(other.nom) == 0)
			{
				return prenom.compareTo(other.prenom);
			}
			return nom.compareTo(other.nom);
		}
		return dateNaissance.compareTo(other.dateNaissance);
	}

	@Override
	public String toString()
	{
		return prenom + " " + nom;
	}

}
