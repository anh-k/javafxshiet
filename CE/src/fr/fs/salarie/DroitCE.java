package fr.fs.salarie;

import java.time.LocalDate;

import fr.fs.outils.OutilsDate;

public class DroitCE
{
	private static final int MOIS_ANCIENNETE = 9;
	private static final String BASE_ANCIENNETE = "01/07/";
	private static final int AGE1 = 10;
	private static final int MONTANT1 = 20;
	private static final int AGE2 = 15;
	private static final int MONTANT2 = 30;
	private static final int AGE3 = 18;
	private static final int MONTANT3 = 50;

	private Salarie salarie;

	public DroitCE(Salarie salarie)
	{
		this.salarie = salarie;
	}

	private boolean chequesVacances()
	{
		LocalDate base = OutilsDate.stringToDate(BASE_ANCIENNETE + LocalDate.now().getYear());
		return base.minusMonths(MOIS_ANCIENNETE).compareTo(salarie.getDateEmbauche()) >= 0;
	}

	private int chequesNoel()
	{
		int montant = 0;
		for (int i = 0; i < salarie.getEnfants().getLength(); i++)
		{
			int age = LocalDate.now().getYear() - salarie.getEnfants().getAt(i).getDateNaissance().getYear();
			if (age <= AGE1)
				montant += MONTANT1;
			else if (age <= AGE2)
				montant += MONTANT2;
			else if (age <= AGE3)
				montant += MONTANT3;
		}
		return montant;
	}

	public void afficher()
	{
		afficherChequesVacance();
		afficherChequesNoel();
		if (salarie.getAgence() != null)
			afficherRestauration();
	}

	private void afficherRestauration()
	{
		if (salarie.getAgence().isRestaurant())
			System.out.printf("%n%n%s a acc�s au Restaurant d'Entreprise", salarie);
		else
			System.out.printf("%n%n%s a droit aux Ch�ques Restaurant", salarie);
	}

	private void afficherChequesNoel()
	{
		System.out.printf("%n%s a droit � %,d euros de ch�ques No�l", salarie, chequesNoel());
	}

	private void afficherChequesVacance()
	{
		if (chequesVacances())
			System.out.printf("%n%n%s a droit aux ch�ques vacances", salarie);
		else
			System.out.printf("%n%n%s n'a pas droit aux ch�ques vacances", salarie);
	}

}
