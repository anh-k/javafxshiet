package fr.fs.outils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class OutilsDate
{
	private OutilsDate()
	{
		
	}
	private static DateTimeFormatter formatNumerique = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	private static DateTimeFormatter formatChaine = DateTimeFormatter.ofPattern("d MMMM yyyy");
	
	public static LocalDate stringToDate(String date)
	{
		return LocalDate.parse(date,formatNumerique);
	}
	public static String dateToString(LocalDate date)
	{
		return formatNumerique.format(date);
	}
	public static String dateToLitteral(LocalDate date)
	{
		return formatChaine.format(date);
	}
	
}
