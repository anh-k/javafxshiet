package vue;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import composantsperso.Main;

public class LoginScreenController {

	private Main mainApp;

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TextField idField;

	@FXML
	private PasswordField pwField;

	@FXML
	private Label errorLabel;

	@FXML
	private Label tryAgainLabel;
	@FXML
	private ImageView xImage;

	@FXML
	void initialize() {
		assert idField != null : "fx:id=\"idField\" was not injected: check your FXML file 'LoginScreen.fxml'.";
		assert pwField != null : "fx:id=\"pwField\" was not injected: check your FXML file 'LoginScreen.fxml'.";

		Tooltip tooltipMail = new Tooltip();
		tooltipMail.setText("Veuillez renseigner un email identifiant");
		idField.setTooltip(tooltipMail);

		Tooltip tooltipPw = new Tooltip();
		tooltipPw.setText("Veuillez saisir le mot de passe correspondant");
		pwField.setTooltip(tooltipPw);
		enableDisableError(false);
	}

	public void setMainApp(Main mainApp) {

		this.mainApp = mainApp;

	}

	private void enableDisableError(boolean b) {
		errorLabel.setVisible(b);
		tryAgainLabel.setVisible(b);
		xImage.setVisible(b);
	}

	@FXML
	void connectionOnAction(ActionEvent event) {
		String identifier = idField.getText();
		String password = pwField.getText();

		System.out.println(identifier);
		System.out.println(password);

		if (mainApp.isValidPassword(password) && mainApp.isValidEmail(identifier)) {

			System.out.println(LocalDate.now() + " " + LocalTime.now().truncatedTo(ChronoUnit.SECONDS));
			enableDisableError(false);

		} else {

			enableDisableError(true);
		}
	}

	@FXML
	void signInOnAction(ActionEvent event) {

		mainApp.showSignIn();

	}

	@FXML
	void resetPwOnAction(ActionEvent event) {

		mainApp.showResetPassword();

	}

}
