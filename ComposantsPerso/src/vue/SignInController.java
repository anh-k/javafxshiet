package vue;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import composantsperso.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class SignInController {
	private Stage dialogStage;
	private Main mainApp;

	@FXML
	private TextField emailFill;

	@FXML
	private TextField pwFill;

	@FXML
	private TextField confirmFill;

	@FXML
	private Button cancelButton;

	@FXML
	private ImageView emailValid;

	@FXML
	private ImageView emailWrong;

	@FXML
	private ImageView pwValid;

	@FXML
	private ImageView pwWrong;

	@FXML
	private ImageView confirmPwValid;

	@FXML
	private ImageView confirmPwWrong;

	@FXML
	private Label pwConditionsLabel;

	@FXML
	void initialize() {

		hideValidIcons(false);

	}

	private void hideValidIcons(boolean b) {
		emailValid.setVisible(b);
		emailWrong.setVisible(b);
		pwValid.setVisible(b);
		pwWrong.setVisible(b);
		confirmPwValid.setVisible(b);
		confirmPwWrong.setVisible(b);
	}

	@FXML
	public void setMainApp(Main mainApp) {
		String pwConditions = "Le mot de passe doit contenir 8 caract�res minimum dont une lettre majuscule, un chiffre et un caract�re sp�cial.";

		pwConditionsLabel.setText(pwConditions);
		this.mainApp = mainApp;
		Tooltip tooltipPw = new Tooltip();
		tooltipPw.setText(
				"8 caract�res minimum\n" + "Une lettre majuscule\n" + "Un chiffre\n" + "Un caract�re sp�cial\n");
		tooltipPw.setAutoHide(false);
		pwFill.setTooltip(tooltipPw);

		Tooltip tooltipMail = new Tooltip();
		tooltipMail.setText("Veuillez renseigner un email valide");
		tooltipMail.setAutoHide(false);
		emailFill.setTooltip(tooltipMail);

	}

	@FXML
	public void closeSignIn(ActionEvent event) {

		dialogStage.close();
		mainApp.showMainapp();
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void checkEmail() {
		String email = emailFill.getText();
		if (email.equals("")) {
			emailValid.setVisible(false);
			emailWrong.setVisible(false);
		} else if (mainApp.isValidEmail(email)) {
			emailValid.setVisible(true);
			emailWrong.setVisible(false);
		} else {
			emailValid.setVisible(false);
			emailWrong.setVisible(true);
		}

	}

	public void checkPassword() {
		String password = pwFill.getText();
		if (password.equals("")) {
			pwValid.setVisible(false);
			pwWrong.setVisible(false);
		} else if (mainApp.isValidPassword(password)) {
			pwValid.setVisible(true);
			pwWrong.setVisible(false);
		} else {
			pwValid.setVisible(false);
			pwWrong.setVisible(true);
		}

	}

	public void checkConfirmPassword() {
		String password = pwFill.getText();
		String confirmPassword = confirmFill.getText();
		if (confirmPassword.equals("")) {
			confirmPwValid.setVisible(false);
			confirmPwWrong.setVisible(false);
		}

		else if (mainApp.isValidPassword(confirmPassword) && confirmPassword.equals(password)) {
			confirmPwValid.setVisible(true);
			confirmPwWrong.setVisible(false);
		} else {

			confirmPwValid.setVisible(false);
			confirmPwWrong.setVisible(true);
		}

	}

	@FXML
	void signInClicked(ActionEvent event) {

		System.out.println(emailFill.getText());
		System.out.println(pwFill.getText());
		System.out.println(confirmFill.getText());
		System.out.println(LocalDate.now() + " " + LocalTime.now().truncatedTo(ChronoUnit.SECONDS));
	}

}
