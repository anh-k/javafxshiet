package vue;

import composantsperso.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class ResetPwController {
	private Main mainApp;

	@FXML
	private Button searchButton;

	@FXML
	private Button cancelButton;

	@FXML
	private TextField searchField;

	public void setMainApp(Main mainApp) {

		this.mainApp = mainApp;

	}

	@FXML
	public void searchEmail(ActionEvent event) {

		String searchEmail = searchField.getText();
		System.out.println(searchEmail);
	}

	@FXML
	public void closeScene(ActionEvent event) {
		mainApp.showMainapp();
	}

}
