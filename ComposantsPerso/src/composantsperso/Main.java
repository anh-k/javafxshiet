package composantsperso;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import vue.LoginScreenController;
import vue.ResetPwController;
import vue.SignInController;
import javafx.scene.image.Image;

public class Main extends Application {

	private Stage stage;
	private AnchorPane layout;
	private Stage modalStage;
	private Scene scene;
	private Pattern mailPattern;
	private Matcher matcher;

	public static void main(String[] args) {

		launch(args);

	}

	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		stage.setTitle("MyCheese 0.1");
		stage.setResizable(false);
		stage.getIcons().add(new Image(getClass().getResourceAsStream("/img/cheese.png")));
		showMainapp();

	}

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public AnchorPane getLayout() {
		return layout;
	}

	public void setLayout(AnchorPane layout) {
		this.layout = layout;
	}

	public void showMainapp() {

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("../vue/LoginScreen.fxml"));
			layout = loader.load();

			// Scene settings
			scene = new Scene(layout);
			stage.setScene(scene);

			// Instancier le controller
			LoginScreenController newController = loader.getController();
			newController.setMainApp(this);

			// Affichage stage
			stage.show();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void showSignIn() {

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("../vue/SignIn.fxml"));
			AnchorPane signInPane = loader.load();

			// Dialog Stage settings
			modalStage = new Stage();
			modalStage.setTitle("MyCheese 0.1");
			modalStage.initModality(Modality.WINDOW_MODAL);
			modalStage.initOwner(stage);
			modalStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/cheese.png")));

			// Scene init
			Scene signInScene = new Scene(signInPane);
			modalStage.setScene(signInScene);
			modalStage.setResizable(false);

			// Instancier le controller
			SignInController newController = loader.getController();
			newController.setMainApp(this);
			newController.setDialogStage(modalStage);

			// Affichage stage
			modalStage.showAndWait();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void showResetPassword() {

		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("../vue/ResetPassword.fxml"));
			AnchorPane resetPwPane = loader.load();

			// Dialog Stage settings

			// Scene init
			Scene resetPwScene = new Scene(resetPwPane);
			stage.setScene(resetPwScene);

			// Instancier le controller
			ResetPwController newController = loader.getController();
			newController.setMainApp(this);

			// Affichage stage
			stage.show();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public boolean isValidEmail(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		Pattern p = Pattern.compile(ePattern);
		Matcher m = p.matcher(email);

		return m.matches();
	}

	public boolean isValidPassword(String password) {
		String pwPattern = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^8+=_-])(?=\\S+$).{8,}$";
		Pattern p = Pattern.compile(pwPattern);
		Matcher m = p.matcher(password);
		return m.matches();
	}

}
